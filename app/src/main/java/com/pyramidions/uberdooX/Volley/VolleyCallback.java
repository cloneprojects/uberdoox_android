package com.pyramidions.uberdooX.Volley;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by user on 18-09-2017.
 */

public interface VolleyCallback {

    public void onSuccess(JSONObject response);


}
