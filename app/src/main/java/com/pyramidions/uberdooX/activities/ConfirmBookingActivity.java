package com.pyramidions.uberdooX.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConfirmBookingActivity extends AppCompatActivity {
    TextView providerName;
    TextView addressText, addressTitle;
    RatingBar ratingView;
    Button confirmBooking;
    TextView pricing, dateValue, timeValue, pricing2;
    String selected_id;
    ImageView backButton;
    ImageView mapData;
    TextView serviceName;
    ImageView providerPic;
    TextView phoneNumber;
    AppSettings appSettings = new AppSettings(ConfirmBookingActivity.this);
    private String TAG = ConfirmBookingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ConfirmBookingActivity.this, "theme_value");
        Utils.Setheme(ConfirmBookingActivity.this, theme_value);
        setContentView(R.layout.activity_confirm_booking);

        initViews();
        initListners();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }


    }

    private void initListners() {
        confirmBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiCall.PostMethodHeaders(ConfirmBookingActivity.this, UrlHelper.NEW_BOOKINGS, getInputs(), new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        showSuccessDialog();
                    }
                });

            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("service_sub_category_id", appSettings.getSelectedSubCategory());
            jsonObject.put("time_slot_id", appSettings.getSelectedTimeSlot());
            jsonObject.put("date", appSettings.getSelectedDate());
            jsonObject.put("provider_id", selected_id);
            jsonObject.put("address_id", appSettings.getSelectedAddressId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void showSuccessDialog() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ConfirmBookingActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_booking_confirmed, null);
            dialogBuilder.setView(dialogView);

            Button okay = dialogView.findViewById(R.id.okay);
            ImageView paymentWaiting = dialogView.findViewById(R.id.paymentWaiting);
            Utils.setIconColour(ConfirmBookingActivity.this, paymentWaiting.getDrawable());
            Utils.setButtonColor(ConfirmBookingActivity.this, okay);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(false);
            okay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    moveToMainActivity();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveToMainActivity() {
        Intent intent = new Intent(ConfirmBookingActivity.this, MainActivity.class);
        intent.putExtra("type", "success");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void initViews() {
        providerName = (TextView) findViewById(R.id.providerName);
//        ratingView = (RatingBar) findViewById(R.id.ratingView);
        dateValue = (TextView) findViewById(R.id.dateValue);
        timeValue = (TextView) findViewById(R.id.timeValue);
        pricing = (TextView) findViewById(R.id.pricing);
        pricing2 = (TextView) findViewById(R.id.pricing2);
        Utils.setTextColour(ConfirmBookingActivity.this, pricing);
        Utils.setTextColour(ConfirmBookingActivity.this, pricing2);
        addressText = (TextView) findViewById(R.id.addresstText);
        addressTitle = (TextView) findViewById(R.id.addressTitle);
        serviceName = (TextView) findViewById(R.id.serviceName);
        confirmBooking = (Button) findViewById(R.id.confirmBooking);
        Utils.setButtonColor(ConfirmBookingActivity.this, confirmBooking);
        providerPic = (ImageView) findViewById(R.id.providerPic);
        mapData = (ImageView) findViewById(R.id.mapData);

        backButton = findViewById(R.id.backButton);

        phoneNumber = (TextView) findViewById(R.id.phoneNumber);

        providerName.setText(getIntent().getStringExtra("providerName"));
        phoneNumber.setText(getIntent().getStringExtra("providerMobile"));
        pricing.setText(getIntent().getStringExtra("pricing"));
//        ratingView.setRating(Float.parseFloat(getIntent().getStringExtra("providerRating")));
        dateValue.setText(appSettings.getSelectedDate());
        timeValue.setText(appSettings.getSelectedTimeText());
        selected_id = getIntent().getStringExtra("selected_id");
        serviceName.setText(appSettings.getSelectedSubCategoryName());
        addressTitle.setText(new StringBuilder()
                .append(getString(R.string.address)).append(": ").append("(")
                .append(appSettings.getSelectedAddressTitle()).append(")"));
        Glide.with(ConfirmBookingActivity.this).load(getIntent().getStringExtra("providerPic")).placeholder(getResources().getDrawable(R.drawable.dp)).into(providerPic);
        getStaticMap(appSettings.getSelectedlat(), appSettings.getSelectedLong());
    }

    private void getStaticMap(String lat, String longg) {

//        String urls="https://maps.googleapis.com/maps/api/staticmap?key="+getResources().getString(R.string.google_maps_key)+"&markers=color:red%7C"+lat+","+longg+"&center="+lat+","+longg+"&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        String urls = "https://maps.googleapis.com/maps/api/staticmap?key=" + getResources().getString(R.string.google_maps_key) + "&center=" + lat + "," + longg + "&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        Utils.log(TAG, "url: " + urls);
//        String url = "http://maps.google.com/maps/api/staticmap?center="+lat+","+longg+"&zoom=15&size=512x512&sensor=false";

        Glide.with(ConfirmBookingActivity.this).load(urls).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                addressText.setText(appSettings.getSelectedAddress());
                return false;
            }
        }).into(mapData);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
