package com.pyramidions.uberdooX.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.fragments.BookingsFragment;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailedBookingActivity extends AppCompatActivity {
    JSONObject bookingValues;
    ImageView backButton;
    LinearLayout billLayout;
    RelativeLayout topBillingLayout;
    RelativeLayout activity_detailed_booking;
    Button statusBackground;
    CircleImageView profilePic;
    TextView billingName, bookingId, bokkingDate, bookingTime, bookingAddress, pricePerhour, serviceName, providerName;
    ImageView mapData;
    TextView totalCostandTime, dummyText, hoursValue;

    TextView statusText;
    View viewOne, viewTwo, viewThree, viewFour, viewFive, viewSix, viewSeven, viewEight, viewNine, viewTen, viewEleven;
    LinearLayout layOne, layTwo, layThree, layFour, layFive, laySix;
    TextView textOne, textTwo, textThree, textFour, textFive, textSix;
    ImageView timeOne, timeTwo, timeThree, timeFour, timeFive, timeSix;
    String statusOne = "", statusTwo = "", statusThree = "", statusFour = "", statusFive = "", statusSix = "";

    Button cancel;
    TextView bookingGst, taxName, taxPercentage;
    private String TAG = DetailedBookingActivity.class.getSimpleName();

    public static String formatHoursAndMinutes(int totalMinutes) {
        String minutes = Integer.toString(totalMinutes % 60);
        if (totalMinutes > 60) {
            minutes = minutes.length() == 1 ? "0" + minutes : minutes;
            if (minutes.equalsIgnoreCase("00")) {
                return (totalMinutes / 60) + " hrs";
            }
            return (totalMinutes / 60) + ":" + minutes + " hrs";
        } else {
            return totalMinutes + " mins";
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(DetailedBookingActivity.this, "theme_value");
        Utils.Setheme(DetailedBookingActivity.this, theme_value);
        setContentView(R.layout.activity_detailed_booking);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        initViews();

        try {
            setValues();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        statusBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (statusBackground.getText().toString().equalsIgnoreCase(getResources().getString(R.string.track))) {
                    trackPage();
                } else {
                    showCancelDialog(bookingValues.optString("id"));
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCancelDialog(bookingValues.optString("id"));

            }
        });

    }

    private void showCancelDialog(final String id) {


        final Dialog dialog = new Dialog(DetailedBookingActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.cancel_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView yesButton, noButton;
        yesButton = (TextView) dialog.findViewById(R.id.yesButton);
        noButton = (TextView) dialog.findViewById(R.id.noButton);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancelRequest(id);
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }












    private void cancelRequest(String id) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(DetailedBookingActivity.this, UrlHelper.CANCEL_BOOKING, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                finish();
            }
        });

    }

    private void trackPage() {

        Intent tracking = new Intent(DetailedBookingActivity.this, TrackingActivity.class);
        tracking.putExtra("des_lat", bookingValues.optString("boooking_latitude"));
        tracking.putExtra("des_lng", bookingValues.optString("booking_longitude"));
        tracking.putExtra("src_lat", bookingValues.optString("provider_latitude"));
        tracking.putExtra("src_lng", bookingValues.optString("provider_longitude"));
        tracking.putExtra("providerName", bookingValues.optString("providername"));
        tracking.putExtra("mobileNumber", bookingValues.optString("provider_mobile"));
        tracking.putExtra("provider_id", bookingValues.optString("provider_id"));
        tracking.putExtra("provider_image", bookingValues.optString("image"));
        startActivity(tracking);
    }


    private void initViews() {

        backButton = (ImageView) findViewById(R.id.backButton);
        billingName = (TextView) findViewById(R.id.billingName);
        hoursValue = (TextView) findViewById(R.id.hoursValue);
        bookingGst = (TextView) findViewById(R.id.bookingGst);
        statusBackground = (Button) findViewById(R.id.statusBackground);
        totalCostandTime = (TextView) findViewById(R.id.totalCostandTime);
        Utils.setTextColour(DetailedBookingActivity.this, hoursValue);
        Utils.setTextColour(DetailedBookingActivity.this, bookingGst);
        Utils.setTextColour(DetailedBookingActivity.this, totalCostandTime);
        Utils.setButtonColor(DetailedBookingActivity.this, statusBackground);
        bookingId = (TextView) findViewById(R.id.bookingId);
        bokkingDate = (TextView) findViewById(R.id.bokkingDate);
//        bookingTime = (TextView) findViewById(R.id.bookingTime);
        bookingAddress = (TextView) findViewById(R.id.bookingAddress);
        pricePerhour = (TextView) findViewById(R.id.pricePerhour);
        serviceName = (TextView) findViewById(R.id.serviceName);
//        providerName = (TextView) findViewById(R.id.providerName);
        billLayout = (LinearLayout) findViewById(R.id.billLayout);
        profilePic = (CircleImageView) findViewById(R.id.profilePic);

        taxName = (TextView) findViewById(R.id.taxName);
        taxPercentage = (TextView) findViewById(R.id.taxPercentage);
        mapData = (ImageView) findViewById(R.id.mapData);
        statusText = (TextView) findViewById(R.id.statusText);

        activity_detailed_booking = (RelativeLayout) findViewById(R.id.activity_detailed_booking);
        topBillingLayout = (RelativeLayout) findViewById(R.id.topBillingLayout);
        cancel = (Button) findViewById(R.id.cancel);

        activity_detailed_booking.setBackgroundColor(getResources().getColor(R.color.transparent));


        textOne = (TextView) findViewById(R.id.textOne);
        textTwo = (TextView) findViewById(R.id.textTwo);
        textThree = (TextView) findViewById(R.id.textThree);
        textFour = (TextView) findViewById(R.id.textFour);
        textFive = (TextView) findViewById(R.id.textFive);
        textSix = (TextView) findViewById(R.id.textSix);

        layOne = (LinearLayout) findViewById(R.id.layOne);
        layTwo = (LinearLayout) findViewById(R.id.layTwo);
        layThree = (LinearLayout) findViewById(R.id.layThree);
        layFour = (LinearLayout) findViewById(R.id.layFour);
        layFive = (LinearLayout) findViewById(R.id.layFive);
        laySix = (LinearLayout) findViewById(R.id.laySix);

        timeOne = (ImageView) findViewById(R.id.timeOne);
        timeTwo = (ImageView) findViewById(R.id.timeTwo);
        timeThree = (ImageView) findViewById(R.id.timeThree);
        timeFour = (ImageView) findViewById(R.id.timeFour);
        timeFive = (ImageView) findViewById(R.id.timeFive);
        timeSix = (ImageView) findViewById(R.id.timeSix);


        viewOne = findViewById(R.id.viewOne);
        viewTwo = findViewById(R.id.viewTwo);
        viewThree = findViewById(R.id.viewThree);
        viewFour = findViewById(R.id.viewFour);
        viewFive = findViewById(R.id.viewFive);
        viewSix = findViewById(R.id.viewSix);
        viewSeven = findViewById(R.id.viewSeven);
        viewEight = findViewById(R.id.viewEight);
        viewNine = findViewById(R.id.viewNine);
        viewTen = findViewById(R.id.viewTen);
        viewEleven = findViewById(R.id.viewEleven);

        viewOne.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewTwo.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewThree.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewFour.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewFive.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewSix.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewSeven.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewEight.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewNine.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewTen.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));
        viewEleven.setBackgroundColor(Utils.getPrimaryCOlor(DetailedBookingActivity.this));

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setValues() throws JSONException, ParseException {
        bookingValues = new JSONObject(getIntent().getStringExtra("bookingValues"));
        Utils.log(TAG, ":bookingValues " + bookingValues);
        String showBill = bookingValues.optString("show_bill_flag");
//        if (!bookingValues.optString("status").equalsIgnoreCase("Finished")) {
        if (showBill.equalsIgnoreCase("0")) {
            topBillingLayout.setVisibility(View.GONE);
            billLayout.setVisibility(View.GONE);

            totalCostandTime.setVisibility(View.GONE);
            billLayout.setVisibility(View.GONE);
            bookingGst.setVisibility(View.GONE);
            taxName.setVisibility(View.GONE);
            taxPercentage.setVisibility(View.GONE);
        } else {
            billLayout.setVisibility(View.VISIBLE);
            topBillingLayout.setVisibility(View.VISIBLE);

            totalCostandTime.setVisibility(View.VISIBLE);
            int total = Integer.parseInt(bookingValues.optString("worked_mins"));
            String hours = formatHoursAndMinutes(total);
            totalCostandTime.setText("$" + bookingValues.optString("total_cost"));
            hoursValue.setText(hours);

//            workingHours.setText(hours);
//            serviceTotal.setText("$" + bookingValues.optString("total_cost"));
        }
//        } else {
//            serviceTotal.setText("$" + bookingValues.optString("cost"));
//            int total = Integer.parseInt(bookingValues.optString("worked_mins"));
//            String hours = formatHoursAndMinutes(total);
//            workingHours.setText(hours);
//
//        }
        billingName.setText(bookingValues.optString("username"));
        bookingId.setText(bookingValues.optString("booking_order_id"));
        bokkingDate.setText(bookingValues.optString("booking_date") + " " + bookingValues.optString("timing"));
//        bookingTime.setText(bookingValues.optString("timing"));
        bookingAddress.setText(bookingValues.optString("address_line_1"));
        pricePerhour.setText("$" + bookingValues.optString("cost"));

        serviceName.setText(bookingValues.optString("sub_category_name"));
//        providerName.setText(bookingValues.optString("providername"));
        bookingGst.setText(bookingValues.optString("gst_cost"));
        taxName.setText(bookingValues.optString("tax_name"));
        taxPercentage.setText(bookingValues.optString("gst_percent") + "%");
        String status = bookingValues.optString("status");
        statusText.setText(status.toUpperCase());


        if (bookingValues.isNull("Pending_time")) {
            statusOne = getResources().getString(R.string.service_booked);

        } else {
            statusOne = getResources().getString(R.string.service_booked) + "\n" + convertedTime(bookingValues.optString("Pending_time"));

        }


        if (bookingValues.isNull("Accepted_time")) {
            statusTwo = getResources().getString(R.string.service_accepted);


        } else {
            statusTwo = getResources().getString(R.string.service_accepted) + "\n" + convertedTime(bookingValues.optString("Accepted_time"));


        }


        if (bookingValues.isNull("StarttoCustomerPlace_time")) {
            statusThree = getResources().getString(R.string.start_to_customer_place);


        } else {
            statusThree = getResources().getString(R.string.start_to_customer_place) + "\n" + convertedTime(bookingValues.optString("StarttoCustomerPlace_time"));

        }

        if (bookingValues.isNull("startjob_timestamp")) {
            statusFour = getResources().getString(R.string.provider_arrived);
            statusFive = getResources().getString(R.string.job_started);


        } else {
            statusFour = getResources().getString(R.string.provider_arrived) + "\n" + convertedTime(bookingValues.optString("startjob_timestamp"));

            statusFive = getResources().getString(R.string.job_started) + "\n" + convertedTime(bookingValues.optString("startjob_timestamp"));

        }

        if (bookingValues.isNull("endjob_timestamp")) {
            statusSix = getResources().getString(R.string.job_completed);


        } else {
            statusSix = getResources().getString(R.string.job_completed) + "\n" + convertedTime(bookingValues.optString("endjob_timestamp"));


        }


        int colorvalue = 0;
        if (status.equalsIgnoreCase("Pending")) {
            statusBackground.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.GONE);
            statusBackground.setText(getResources().getString(R.string.cancel));
//            statusBackground.setVisibility(View.GONE);
            setBooked();
            colorvalue = getResources().getColor(R.color.light_violet);
            statusText.setText(getResources().getString(R.string.pending));


        } else if (status.equalsIgnoreCase("Rejected")) {
            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


            statusTwo = getResources().getString(R.string.service_rejected) + "\n" + bookingValues.optString("Rejected_time");

            setAccepted();
            colorvalue = getResources().getColor(R.color.red);
            statusText.setText(getResources().getString(R.string.rejected));


        } else if (status.equalsIgnoreCase("Accepted")) {
            statusText.setText(getResources().getString(R.string.accepted));

            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.VISIBLE);


            setAccepted();
            colorvalue = getResources().getColor(R.color.light_violet);


        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


            if (bookingValues.isNull("CancelledbyUser_time")) {
                statusThree = getResources().getString(R.string.cancelled_by_user);


            } else {
                statusThree = getResources().getString(R.string.cancelled_by_user) + "\n" + bookingValues.optString("CancelledbyUser_time");


            }


            setStartToPlace();
            colorvalue = getResources().getColor(R.color.red);
            statusText.setText(getResources().getString(R.string.cancelled));

        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
            statusBackground.setVisibility(View.GONE);

            cancel.setVisibility(View.GONE);


            if (bookingValues.isNull("CancelledbyProvider_time")) {
                statusThree = getResources().getString(R.string.cancelled_by_provider);


            } else {
                statusThree = getResources().getString(R.string.cancelled_by_provider) + "\n" + bookingValues.optString("CancelledbyProvider_time");


            }

            setStartToPlace();
            colorvalue = getResources().getColor(R.color.red);
            statusText.setText(getResources().getString(R.string.cancelled));


        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {
            statusBackground.setText(getResources().getString(R.string.track));
            cancel.setVisibility(View.GONE);


            statusBackground.setVisibility(View.VISIBLE);

            setStartToPlace();
            statusBackground.setVisibility(View.VISIBLE);
            colorvalue = getResources().getColor(R.color.light_violet);
            statusText.setText(getResources().getString(R.string.on_the_way));


        } else if (status.equalsIgnoreCase("Startedjob")) {

            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


            setJobStarted();

            colorvalue = getResources().getColor(R.color.light_violet);
            statusText.setText(getResources().getString(R.string.working));

        } else if (status.equalsIgnoreCase("Completedjob")) {
            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


            setJobCompleted();

            colorvalue = getResources().getColor(R.color.green);
            statusText.setText(getResources().getString(R.string.completed));
        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


            colorvalue = getResources().getColor(R.color.status_orange);
            statusBackground.setVisibility(View.GONE);

        } else if (status.equalsIgnoreCase("Reviewpending")) {
            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


            colorvalue = getResources().getColor(R.color.ratingColor);
            statusBackground.setVisibility(View.GONE);

        } else if (status.equalsIgnoreCase("Finished")) {
            statusBackground.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);


            setJobCompleted();

            colorvalue = getResources().getColor(R.color.green);
            statusText.setText(getResources().getString(R.string.completed));
        }

        statusText.setTextColor(colorvalue);


        getStaticMap(bookingValues.optString("boooking_latitude"), bookingValues.optString("booking_longitude"));

    }

    public String convertedTime(String time) throws ParseException {
        String ftime = "";
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date outDate = dateFormatter.parse(time);
        DateFormat formatter = new SimpleDateFormat("d MMM, yyyy HH:mm a");
        ftime = formatter.format(outDate);

        return ftime;
    }


    public void setBooked() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.GONE);
        layThree.setVisibility(View.GONE);
        layFour.setVisibility(View.GONE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

        //timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));

        Utils.setBacColour(DetailedBookingActivity.this, timeOne);
        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.INVISIBLE);
        viewTwo.setVisibility(View.INVISIBLE);
        viewThree.setVisibility(View.INVISIBLE);
        viewFour.setVisibility(View.INVISIBLE);
        viewFive.setVisibility(View.INVISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);


    }


    public void setAccepted() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.GONE);
        layFour.setVisibility(View.GONE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

        Utils.setViewDot(DetailedBookingActivity.this, timeOne);
        // timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        Utils.setBacColour(DetailedBookingActivity.this, timeTwo);
        //timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.INVISIBLE);
        viewFour.setVisibility(View.INVISIBLE);
        viewFive.setVisibility(View.INVISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);
    }

    public void setStartToPlace() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.GONE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

        // timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        //timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));

        //timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));

        Utils.setViewDot(DetailedBookingActivity.this, timeOne);
        Utils.setViewDot(DetailedBookingActivity.this, timeTwo);

        Utils.setBacColour(DetailedBookingActivity.this, timeThree);

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.INVISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);
    }


    public void setProviderArrived() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.VISIBLE);
        layFive.setVisibility(View.GONE);
        laySix.setVisibility(View.GONE);

        /*timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));*/
        //timeFour.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));

        Utils.setViewDot(DetailedBookingActivity.this, timeOne);
        Utils.setViewDot(DetailedBookingActivity.this, timeTwo);
        Utils.setViewDot(DetailedBookingActivity.this, timeThree);
        Utils.setBacColour(DetailedBookingActivity.this, timeFour);

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.VISIBLE);
        viewSix.setVisibility(View.INVISIBLE);
        viewSeven.setVisibility(View.INVISIBLE);
        viewEight.setVisibility(View.INVISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);
    }

    public void setJobStarted() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.VISIBLE);
        layFive.setVisibility(View.VISIBLE);
        laySix.setVisibility(View.GONE);

       /* timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFour.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));*/

        Utils.setViewDot(DetailedBookingActivity.this, timeOne);
        Utils.setViewDot(DetailedBookingActivity.this, timeTwo);
        Utils.setViewDot(DetailedBookingActivity.this, timeThree);
        Utils.setViewDot(DetailedBookingActivity.this, timeFour);

        Utils.setBacColour(DetailedBookingActivity.this, timeFive);
        //timeFive.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.VISIBLE);
        viewSix.setVisibility(View.VISIBLE);
        viewSeven.setVisibility(View.VISIBLE);
        viewEight.setVisibility(View.VISIBLE);
        viewNine.setVisibility(View.INVISIBLE);
        viewTen.setVisibility(View.INVISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);
    }


    public void setJobCompleted() {
        layOne.setVisibility(View.VISIBLE);
        layTwo.setVisibility(View.VISIBLE);
        layThree.setVisibility(View.VISIBLE);
        layFour.setVisibility(View.VISIBLE);
        layFive.setVisibility(View.VISIBLE);
        laySix.setVisibility(View.VISIBLE);

       /* timeOne.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeTwo.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeThree.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFour.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));
        timeFive.setImageDrawable(getResources().getDrawable(R.drawable.circle_violeet_transparent));*/

        Utils.setViewDot(DetailedBookingActivity.this, timeOne);
        Utils.setViewDot(DetailedBookingActivity.this, timeTwo);
        Utils.setViewDot(DetailedBookingActivity.this, timeThree);
        Utils.setViewDot(DetailedBookingActivity.this, timeFour);
        Utils.setViewDot(DetailedBookingActivity.this, timeFive);

        Utils.setBacColour(DetailedBookingActivity.this, timeSix);
        //timeSix.setImageDrawable(getResources().getDrawable(R.drawable.circle_violet));

        textOne.setText(statusOne);
        textTwo.setText(statusTwo);
        textThree.setText(statusThree);
        textFour.setText(statusFour);
        textFive.setText(statusFive);
        textSix.setText(statusSix);


        viewOne.setVisibility(View.VISIBLE);
        viewTwo.setVisibility(View.VISIBLE);
        viewThree.setVisibility(View.VISIBLE);
        viewFour.setVisibility(View.VISIBLE);
        viewFive.setVisibility(View.VISIBLE);
        viewSix.setVisibility(View.VISIBLE);
        viewSeven.setVisibility(View.VISIBLE);
        viewEight.setVisibility(View.VISIBLE);
        viewNine.setVisibility(View.VISIBLE);
        viewTen.setVisibility(View.VISIBLE);
        viewEleven.setVisibility(View.INVISIBLE);
    }


    private void getStaticMap(String lat, String longg) {

//        String urls="https://maps.googleapis.com/maps/api/staticmap?key="+getResources().getString(R.string.google_maps_key)+"&markers=color:red%7C"+lat+","+longg+"&center="+lat+","+longg+"&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        String urls = "https://maps.googleapis.com/maps/api/staticmap?key=" + getResources().getString(R.string.google_maps_key) + "&center=" + lat + "," + longg + "&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        Utils.log(TAG, "url: " + urls);
//        String url = "http://maps.google.com/maps/api/staticmap?center="+lat+","+longg+"&zoom=15&size=512x512&sensor=false";

        Glide.with(DetailedBookingActivity.this).load(urls).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                return false;
            }
        }).into(mapData);


    }


}
