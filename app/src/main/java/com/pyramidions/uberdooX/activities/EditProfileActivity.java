package com.pyramidions.uberdooX.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    EditText firstName, lastName, mobileNumber;
    Button bottomButton;
    ImageView profileImage;
    ImageView backButton;
    private Uri uri;
    private File uploadFile;
    private final int cameraIntent = 2;

    private String uploadImagepath;
    private String TAG = EditProfileActivity.class.getSimpleName();
    private AppSettings appSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(EditProfileActivity.this, "theme_value");
        Utils.Setheme(EditProfileActivity.this, theme_value);
        setContentView(R.layout.activity_edit_profile);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }


        initViews();
        initListners();
        try {
            setValues();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initListners() {
        bottomButton.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void setValues() throws JSONException {
        JSONObject generalObject = new JSONObject(getIntent().getStringExtra("provider_details"));
        firstName.setText(generalObject.optString("first_name"));
        lastName.setText(generalObject.optString("last_name"));
        mobileNumber.setText(generalObject.optString("mobile"));

        uploadImagepath = generalObject.optString("image");
        Drawable drawable = getResources().getDrawable(R.drawable.dp);
        drawable.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(EditProfileActivity.this),
                PorterDuff.Mode.SRC_IN));

        Glide.with(this).load(uploadImagepath)
                .placeholder(drawable).dontAnimate().into(profileImage);

    }

    private void initViews() {
        appSettings = new AppSettings(EditProfileActivity.this);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        profileImage = (ImageView) findViewById(R.id.profileImage);
        //Utils.setIconColour(EditProfileActivity.this, profileImage.getDrawable());
        bottomButton = (Button) findViewById(R.id.bottomButton);
        Utils.setButtonColor(EditProfileActivity.this, bottomButton);
        backButton = (ImageView) findViewById(R.id.backButton);
    }


    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }

    private void takePhotoFromCamera() {
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(),
                getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder,
                getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }

        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

//        uri = Uri.fromFile(file);

//        uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(EditProfileActivity.this, getPackageName() + ".provider", file);
            Log.d(TAG, "onClick: " + uri.getPath());
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        } else {
            uri = Uri.fromFile(file);
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        }
        Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());

        /*Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 2);*/
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.storage_permission_error));

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1://gallery
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        handleimage(uri);
                    } else {
                        Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_image));
                    }
                }
                break;
            case cameraIntent://camera
                uploadFile = new File(String.valueOf(appSettings.getImageUploadPath()));
                if (uploadFile.exists()) {
                    Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        profileImage.setColorFilter(ContextCompat.getColor(EditProfileActivity.this, R.color.transparent));
                        Glide.with(EditProfileActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profileImage);
                    } else {
                        profileImage.setColorFilter(getResources().getColor(R.color.transparent));
                        Glide.with(EditProfileActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profileImage);
                    }
                }
                break;


        }
    }

    private void handleimage(Uri uri) {

        profileImage.setColorFilter(getResources().getColor(R.color.transparent));
        Glide.with(EditProfileActivity.this)
                .load(Utils.getRealPathFromUriNew(EditProfileActivity.this, uri))
                .into(profileImage);
        uploadFile = new File(Utils.getRealPathFromURI(EditProfileActivity.this, uri));

    }


    @Override
    public void onClick(View view) {
        if (view == profileImage) {
            showPictureDialog();
        } else if (view == bottomButton) {
            if (uploadFile == null) {
                try {
                    Utils.show(EditProfileActivity.this);
                    registerValues();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                uploadImage();
            }
        } else if (view == backButton) {
            finish();
        }
    }

    private void uploadImage() {
        ApiCall.uploadImage(uploadFile, EditProfileActivity.this, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                uploadImagepath = response.optString("image");
                try {
                    registerValues();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void registerValues() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("first_name", firstName.getText().toString());
        jsonObject.put("last_name", lastName.getText().toString());
        jsonObject.put("mobile", mobileNumber.getText().toString());
        jsonObject.put("image", uploadImagepath);

        ApiCall.PostMethodHeadersNoProgress(EditProfileActivity.this, UrlHelper.UPDATE_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                // Utils.dismiss();
                Log.d("checking", "onSuccess:inputs " + jsonObject);
                Log.d("checking", "onSuccess:Res " + response);

                runOnUiThread(new Runnable() {
                    public void run() {
                        Utils.toast(EditProfileActivity.this, getResources().getString(R.string.profile_updated_successfully));
                    }
                });

                moveMainActivity();
            }
        });


    }

    private void moveMainActivity() {
        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
        intent.putExtra("type", "new");
        startActivity(intent);
    }
}
