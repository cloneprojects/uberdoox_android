package com.pyramidions.uberdooX.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.pyramidions.uberdooX.helpers.Utils.isValidEmail;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    Button sendOtp;
    EditText emailEditText;
    ImageView backButton;

//    public final static boolean isValidEmail(CharSequence target) {
//        if (target == null) {
//            return false;
//        } else {
//            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ForgotPasswordActivity.this, "theme_value");
        Utils.Setheme(ForgotPasswordActivity.this, theme_value);
        setContentView(R.layout.activity_forgot_password);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        initViews();
        initListners();
    }

    private void initListners() {
        backButton.setOnClickListener(this);
        sendOtp.setOnClickListener(this);

    }

    private void initViews() {
        backButton = (ImageView) findViewById(R.id.backButton);
        sendOtp = (Button) findViewById(R.id.sendOtp);
        Utils.setButtonColor(ForgotPasswordActivity.this, sendOtp);
        Button email_button = findViewById(R.id.email_button);
        Utils.setButton(ForgotPasswordActivity.this, email_button);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {

        if (view == backButton) {
            finish();
        } else if (view == sendOtp) {
            if (isValidInputs().equalsIgnoreCase("true")) {
                requestOtp();
            } else {
                Utils.toast(ForgotPasswordActivity.this, isValidInputs());
            }
        }
    }

    private String isValidInputs() {

        String val;
        if (emailEditText.getText().toString().trim().length() == 0 || !isValidEmail(emailEditText.getText().toString().trim())) {
            val = getResources().getString(R.string.please_enter_valid_email);
        } /*else if (emailEditText.getText().toString().equalsIgnoreCase("karthik@pyrmaidions.com")) {
            val = getResources().getString(R.string.you_cannot_change_password);

        }*/ else {
            val = "true";
        }
        return val;
    }

    private void requestOtp() {
        ApiCall.PostMethod(ForgotPasswordActivity.this, UrlHelper.FORGOT_PASSWORD, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                String otp = response.optString("otp");
                moveVerfication(otp);
            }


        });

    }

    private void moveVerfication(String otp) {
        Intent intent = new Intent(ForgotPasswordActivity.this, EnterVerificationCodeActivity.class);
        intent.putExtra("otp", otp);
        intent.putExtra("emailValue", emailEditText.getText().toString().trim());
        startActivity(intent);
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", emailEditText.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
