package com.pyramidions.uberdooX.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.pyramidions.uberdooX.Events.Status;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Stripe.ExampleEphemeralKeyProvider;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.adapters.PaymentAdapter;
import com.pyramidions.uberdooX.fragments.AccountsFragment;
import com.pyramidions.uberdooX.fragments.BookingsFragment;
import com.pyramidions.uberdooX.fragments.HomeFragment;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.model.Customer;
import com.stripe.android.model.CustomerSource;
import com.stripe.android.view.PaymentMethodsActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements BookingsFragment.OnFragmentInteractionListener, HomeFragment.OnFragmentInteractionListener, AccountsFragment.OnFragmentInteractionListener {

    private static final int REQUEST_CODE_SELECT_SOURCE = 55;
    public static JSONObject userDetails = new JSONObject();
    public static Boolean isCardSelected = true;
    public static RelativeLayout cardLayout;
    Boolean isPaymentPending = true;
    AppSettings appSettings = new AppSettings(MainActivity.this);
    String cardID;
    TextView cardName, optionButton;
    boolean isPaymentEnabled = false;
    private boolean isCommentVisible = false;
    private String TAG = MainActivity.class.getSimpleName();
    private Dialog dialog;

    public static void clearLightStatusBar(Activity activity, View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            Window window = activity.getWindow();
            window.setStatusBarColor(ContextCompat
                    .getColor(activity, R.color.colorPrimaryDark));

        }
    }

    public static void setLightStatusBar(View view, Activity activity) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            activity.getWindow().setStatusBarColor(Color.BLACK);
        }
    }

    public static String formatHoursAndMinutes(int totalMinutes) {
        String minutes = Integer.toString(totalMinutes % 60);
        minutes = minutes.length() == 1 ? "0" + minutes : minutes;
        if (minutes.equalsIgnoreCase("00")) {
            return (totalMinutes / 60) + " hrs";
        }
        return (totalMinutes / 60) + ":" + minutes + " hrs";
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAppSettings();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(MainActivity.this, "theme_value");
        Utils.Setheme(MainActivity.this, theme_value);
        setContentView(R.layout.activity_main);
        PaymentConfiguration.init(getResources().getString(R.string.stripe_key));

        ViewPager mainPager = (ViewPager) findViewById(R.id.mainPager);
//        CustomNavigationTabStrip tabStrip = (CustomNavigationTabStrip)findViewById(R.id.tabStrip);
        NavigationTabStrip tabStrip = (NavigationTabStrip) findViewById(R.id.tabStrip);
        Utils.setStripColor(MainActivity.this, tabStrip);
        String[] titles = new String[]{getResources().getString(R.string.bookings), getResources().getString(R.string.home), getResources().getString(R.string.account)};
        tabStrip.setTitles(titles);
        try {
            SelectTimeAndAddressActivity.isAddressSelected = false;
            SelectTimeAndAddressActivity.isDateSeleceted = false;
            SelectTimeAndAddressActivity.isTimeSelected = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        getAppSettings();


//        showInvoice();

//        setLightStatusBar(getWindow().getDecorView(),this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        MainPagerAdapter mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mainPager.setAdapter(mainPagerAdapter);
        if (getIntent().getStringExtra("type").equalsIgnoreCase("success")) {
            mainPager.setCurrentItem(0);
        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("theme_changed")) {
            mainPager.setCurrentItem(2);
        } else {
            mainPager.setCurrentItem(1);
        }


        mainPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("Page Scrolled", positionOffset + "," + positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabStrip.setViewPager(mainPager);

        if (appSettings.getUserType().equalsIgnoreCase("guest")) {

        } else {
            getRegularValues();
        }
    }

    public void getAppSettings() {


        ApiCall.getMethodHeaders(MainActivity.this, UrlHelper.APP_SETTINGS, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                JSONArray jsonArray = new JSONArray();
                JSONArray status = new JSONArray();
                jsonArray = response.optJSONArray("timeslots");
                status = response.optJSONArray("status");
                Utils.log(TAG, "timeslits:" + jsonArray);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = jsonArray.optJSONObject(i);
                    try {
                        jsonObject.put("selected", "false");
                        jsonArray.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                appSettings.setTimeSlots(jsonArray);

                try {
                    if (status.length() > 0) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject = status.optJSONObject(0);
                        String statusvalue = jsonObject.optString("status");
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (statusvalue.equalsIgnoreCase("Blocked")) {
                            showBlocked();
                        } else if (statusvalue.equalsIgnoreCase("Dispute")) {
                            showDispute();
                        } else if (statusvalue.equalsIgnoreCase("Completedjob")) {
                            showInvoice(jsonObject);
                        } else if (statusvalue.equalsIgnoreCase("Reviewpending")) {
                            showRating(jsonObject);
                        } else if (statusvalue.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
                            showWaitingforPayment();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }

    private void getRegularValues() {
        ApiCall.getMethodHeaders(MainActivity.this, UrlHelper.VIEW_PROFILE, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                userDetails = response.optJSONObject("user_details");
                String fullname = userDetails.optString("first_name") + " " + userDetails.optString("last_name");
                String userMobile = userDetails.optString("mobile");
                String userImage = userDetails.optString("image");

                appSettings.setUserImage(userDetails.optString("image"));
                appSettings.setUserName(userDetails.optString("first_name")
                        + " " + userDetails.optString("last_name"));
                appSettings.setUserNumber(userDetails.optString("mobile"));

//                HomeFragment.userName.setText(fullname);


                try {
                    AccountsFragment.userName.setText(fullname);
                    AccountsFragment.userMobile.setText(userMobile);
                    Glide.with(MainActivity.this)
                            .load(userImage)
                            .placeholder(getResources().getDrawable(R.drawable.dp))
                            .into(AccountsFragment.profilePic);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        ApiCall.PostMethodHeaders(MainActivity.this, UrlHelper.UPDATE_DEVICE_TOKEN, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {


            }
        });
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fcm_token", appSettings.getFireBaseToken());
            jsonObject.put("os", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private void launchWithCustomer() {
        Intent payIntent = PaymentMethodsActivity.newIntent(this);
        startActivityForResult(payIntent, REQUEST_CODE_SELECT_SOURCE);
    }

    private void postToken(String token, String booking_id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", token);
        jsonObject.put("id", booking_id);

        ApiCall.PostMethodHeaders(MainActivity.this, UrlHelper.STRIPE_PAYMENT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                getAppSettings();
            }
        });
    }


    public void showInvoice(JSONObject bookingValues) {

        dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.invoice_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();
        cardName = dialog.findViewById(R.id.cardName);
        optionButton = dialog.findViewById(R.id.optionButton);
        Utils.setTextColour(MainActivity.this, optionButton);
        initCustomerSession();


        final ImageView card_selected_border, cash_selected_border, card_icon, cash_icon;
        final FrameLayout card_selected_round, cash_selected_round;
        CardView card, cash;
        RecyclerView paymentMethod;
        TextView providerName, billingName, bookingId, bookingDate, bookingTime, workedHours, bookingPrice, bookingGst, bookingTotal, taxName, taxPercentage;
        Button confirm;

        confirm = (Button) dialog.findViewById(R.id.confirm);
        Utils.setButtonColor(MainActivity.this, confirm);
        paymentMethod = (RecyclerView) dialog.findViewById(R.id.paymentMethod);
        cardLayout = dialog.findViewById(R.id.cardLayout);


        providerName = (TextView) dialog.findViewById(R.id.providerName);
        billingName = (TextView) dialog.findViewById(R.id.billingName);
        bookingId = (TextView) dialog.findViewById(R.id.bookingId);
        bookingDate = (TextView) dialog.findViewById(R.id.bookingDate);
        bookingTime = (TextView) dialog.findViewById(R.id.bookingTime);
        workedHours = (TextView) dialog.findViewById(R.id.workedHours);
        bookingPrice = (TextView) dialog.findViewById(R.id.bookingPrice);
        bookingTotal = (TextView) dialog.findViewById(R.id.bookingTotal);
        bookingGst = (TextView) dialog.findViewById(R.id.bookingGst);
        taxName = (TextView) dialog.findViewById(R.id.taxName);
        taxPercentage = (TextView) dialog.findViewById(R.id.taxPercentage);
        setRecyclerView(paymentMethod);
        try {
            if (bookingValues.optString("worked_mins") != null) {
                int total = Integer.parseInt(bookingValues.optString("worked_mins"));
                String hours = formatHoursAndMinutes(total);
                workedHours.setText(hours);
            }
        } catch (Exception e) {
        }


        bookingTotal.setText("$" + bookingValues.optString("total_cost"));
        billingName.setText(bookingValues.optString("username"));
        bookingId.setText(bookingValues.optString("booking_order_id"));
        bookingDate.setText(bookingValues.optString("booking_date"));
        bookingTime.setText(bookingValues.optString("timing"));
        bookingPrice.setText(bookingValues.optString("cost"));
        providerName.setText(bookingValues.optString("providername"));
        bookingGst.setText(bookingValues.optString("gst_cost"));
        taxName.setText(bookingValues.optString("tax_name"));
        taxPercentage.setText(bookingValues.optString("gst_percent") + "%");


        final String booking_id = bookingValues.optString("booking_id");


        optionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isPaymentEnabled = true;
                launchWithCustomer();

            }
        });


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.show(MainActivity.this);
                if (isCardSelected) {

                    if (cardID.length() != 0) {
                        try {
                            postToken(cardID, booking_id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.dismiss();

                        Utils.toast(MainActivity.this, getResources().getString(R.string.please_select_any_card));
                    }

                } else {
                    try {
                        hitPaymentMethod(booking_id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


    }

    private void setRecyclerView(final RecyclerView paymentMethod) {
        ApiCall.getMethodHeaders(MainActivity.this, UrlHelper.PAYMENT_METHODS, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray jsonArray = new JSONArray();
                jsonArray = response.optJSONArray("payment_types");
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        if (i == 0) {
                            jsonArray.optJSONObject(i).put("selected", "true");
                        } else {
                            jsonArray.optJSONObject(i).put("selected", "false");
                        }
                    } catch (Exception e) {

                    }
                }
                PaymentAdapter paymentAdapter = new PaymentAdapter(MainActivity.this, jsonArray);
                LinearLayoutManager linearLayoutManager =
                        new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
                paymentMethod.setLayoutManager(linearLayoutManager);
                paymentMethod.setAdapter(paymentAdapter);
                paymentAdapter.notifyDataSetChanged();
            }
        });
    }

    private void hitPaymentMethod(String booking_id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("method", appSettings.getPaymentType());
        jsonObject.put("id", booking_id);
        ApiCall.PostMethodHeaders(MainActivity.this, UrlHelper.PAYMENT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                getAppSettings();
            }
        });
    }

    public void showWaitingforPayment() {

        try {
            dialog.dismiss();
        } catch (Exception e) {

        }


        dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_payment_confirmation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();
        final CircleImageView confirmed = (CircleImageView) dialog.findViewById(R.id.paymentConfirmed);
        ImageView payConfImage = (CircleImageView) dialog.findViewById(R.id.payConfImage);
        final ImageView paymentWaiting = (ImageView) dialog.findViewById(R.id.paymentWaiting);
        final TextView payConfText = (TextView) dialog.findViewById(R.id.payConfText);
        Drawable drawable1 = getResources().getDrawable(R.drawable.text_circular);
        drawable1.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(MainActivity.this),
                PorterDuff.Mode.SRC_IN));
        payConfImage.setBackground(drawable1);
    }


    public void showThanks() {

        dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.show_thanks);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();

        final CircleImageView confirmed = (CircleImageView) dialog.findViewById(R.id.paymentConfirmed);
        final ImageView paymentWaiting = (ImageView) dialog.findViewById(R.id.paymentWaiting);
        final TextView payConfText = (TextView) dialog.findViewById(R.id.payConfText);
        Button confirm = (Button) dialog.findViewById(R.id.confirm);
        paymentWaiting.setVisibility(View.VISIBLE);
        Utils.setButtonColor(MainActivity.this, confirm);
        Utils.setIconColour(MainActivity.this, paymentWaiting.getDrawable());
        confirmed.setVisibility(View.GONE);
        payConfText.setText(getResources().getString(R.string.thanks_for));

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAppSettings();
                BookingsFragment.getData(MainActivity.this);
                dialog.dismiss();
            }
        });

    }


    private void showRating(final JSONObject jsonObject) {

        dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_rating);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        Button submit = (Button) dialog.findViewById(R.id.submit);
        Utils.setButtonColor(MainActivity.this, submit);
        final LinearLayout topLayout = (LinearLayout) dialog.findViewById(R.id.topLayout);

        final EditText feedBackText = (EditText) dialog.findViewById(R.id.feedBackText);
        final LinearLayout commentSection = (LinearLayout) dialog.findViewById(R.id.commentSection);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (!isCommentVisible) {
                    animateUp(commentSection, topLayout);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    postValues(ratingBar.getRating(), jsonObject.optString("booking_id"), feedBackText.getText().toString(), jsonObject.optString("provider_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void postValues(float rating, String booking_id, String s, String provider_id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        int f_rating = Math.round(rating);
        jsonObject.put("booking_id", booking_id);
        jsonObject.put("rating", "" + f_rating);
        jsonObject.put("id", provider_id);
        jsonObject.put("feedback", s);
        ApiCall.PostMethodHeaders(MainActivity.this, UrlHelper.REVIEW, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                isCommentVisible = false;
                dialog.dismiss();
                showThanks();
            }
        });
    }

    public void showBlocked() {

        dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_blocked);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStatus(Status event) {
        getAppSettings();
    }


    public void showDispute() {
//        dialog = getLayoutInflater().inflate(R.layout.view_dispute, null);
//        dialog = new dialog(MainActivity.this);
//        dialog.setContentView(dialog);
//        bottomSheetBehavior = BottomSheetBehavior.from((View) dialog.getParent());
//        dialog.show();
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//
//        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//            }
//        });


        dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_blocked);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Status event) {

        getAppSettings();

    }

    private String buildCardString(@NonNull String brand, String cardLast) {
        return brand + getString(R.string.ending_in) + cardLast;
    }


    public void initCustomerSession() {
        CustomerSession.initCustomerSession(
                new ExampleEphemeralKeyProvider(
                        new ExampleEphemeralKeyProvider.ProgressListener() {
                            @Override
                            public void onStringResponse(String string) {
                                if (string.startsWith("Error: ")) {

                                }
                            }
                        }, MainActivity.this));

        CustomerSession.getInstance().retrieveCurrentCustomer(
                new CustomerSession.CustomerRetrievalListener() {
                    @Override
                    public void onCustomerRetrieved(@NonNull Customer customer) {
                        try {
                            CustomerSource sourcee = customer.getSourceById(customer.getDefaultSource());
                            String selectedSource = sourcee.toString();
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject = new JSONObject(selectedSource);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (jsonObject.optString("livemode").equalsIgnoreCase("true")) {
                                if (jsonObject.length() > 0) {
                                    cardID = jsonObject.optString("id");
                                    Log.e(TAG, "onCustomerRetrieved: " + cardID);
                                    cardName.setText(buildCardString(jsonObject.optString("brand"), jsonObject.optString("last4")));
                                    optionButton.setText(getResources().getString(R.string.change));

                                } else {
                                    cardID = "";
                                    cardName.setText("");
                                    optionButton.setText(getResources().getString(R.string.add_card_text));

                                }
                            } else {

                                if (jsonObject.length() > 0) {
                                    cardID = jsonObject.optString("id");
                                    Log.e(TAG, "onCustomerRetrieved: " + cardID);
                                    cardName.setText(buildCardString(jsonObject.optJSONObject("card").optString("brand"), jsonObject.optJSONObject("card").optString("last4")));
                                    optionButton.setText(getResources().getString(R.string.change));

                                } else {
                                    cardID = "";
                                    cardName.setText("");
                                    optionButton.setText(getResources().getString(R.string.add_card_text));

                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            cardID = "";
                            cardName.setText("");
                            optionButton.setText(getResources().getString(R.string.add_card_text));
                        }
                    }

                    @Override
                    public void onError(int errorCode, @Nullable String errorMessage) {
                        Log.d("chek", "error: ");


                    }
                });


    }


    private void animateUp(LinearLayout commentSection, final LinearLayout topLayout) {
        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) topLayout.getLayoutParams();
        params.bottomMargin = 55;
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.slidefrombottomm);
        isCommentVisible = true;
        commentSection.setVisibility(View.VISIBLE);
        commentSection.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.slidefrombottom));

        topLayout.setAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                topLayout.setLayoutParams(params);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class MainPagerAdapter extends FragmentStatePagerAdapter {


        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return BookingsFragment.newInstance(getResources().getString(R.string.bookings), "Fragment");
                case 1:
                    return HomeFragment.newInstance(getResources().getString(R.string.home), "Fragment");
                case 2:
                    return AccountsFragment.newInstance(getResources().getString(R.string.account), "Fragment");
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
