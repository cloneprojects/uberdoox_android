package com.pyramidions.uberdooX.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.helpers.Callback;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    String dist = "";

    GoogleMap map;
    LinearLayout editAddress;
    Button confirmButton;
    ImageView myLocation;
    ImageView backButton;
    EditText locationName;
    EditText title, doorNo, landMark;
    ImageView markerIcon;
    SupportMapFragment mapFragment;
    LinearLayout addAddressButton, editLayout;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private String TAG = MapActivity.class.getSimpleName();
    private boolean isAddressShown = false;
    private int value = 0;
    private String cityName = "";
    RelativeLayout mapParent;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(MapActivity.this, "theme_value");
        Utils.Setheme(MapActivity.this, theme_value);
        setContentView(R.layout.activity_map);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        initlocation();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            settingsrequest();
            initlocationRequest();
        }


        initViews();
        initListners();


    }


    private void initlocation() {
        checkCallingOrSelfPermission("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }


    private void initlocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MapActivity.this, 5);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    private void initListners() {

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidInputs().equalsIgnoreCase("true")) {
                    if (type.equalsIgnoreCase("edit")) {
                        updateAddress();
                    } else {
                        saveAddress();
                    }
                } else {
                    Utils.toast(MapActivity.this, isValidInputs());
                }
            }
        });

        addAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAddressShown) {
                    slideDown();
                } else {
                    slideUp();
                }
            }
        });

    }

    private void updateAddress() {
        ApiCall.PostMethodHeaders(MapActivity.this, UrlHelper.UPDATE_ADDRESS, getUpdateInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(MapActivity.this, getResources().getString(R.string.updated_successfully));
                Intent timeAndAddress = new Intent(MapActivity.this, AddressActivity.class);
                timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(timeAndAddress);
            }
        });
    }


    private void slideDown() {
        isAddressShown = false;
        Animation animation = AnimationUtils.loadAnimation(MapActivity.this, R.anim.slidetobottom);
        editLayout.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                editLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void slideUp() {
        isAddressShown = true;
        editLayout.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(MapActivity.this, R.anim.slidefrombottom);
        editLayout.startAnimation(animation);


    }


    private String isValidInputs() {
        String val;
        if (locationName.getText().toString().length() == 0) {
            val = getResources().getString(R.string.enter_valid_address);
        } else if (doorNo.getText().toString().length() == 0) {
            val = getResources().getString(R.string.enter_valid_door_no);
        } else if (landMark.getText().toString().length() == 0) {
            val = getResources().getString(R.string.enter_valid_landmark);
        } else if (title.getText().toString().length() == 0) {
            val = getResources().getString(R.string.enter_valid_title);
        } else {
            val = "true";
        }
        return val;
    }

    private void saveAddress() {
        ApiCall.PostMethodHeaders(MapActivity.this, UrlHelper.ADD_ADDRESS, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(MapActivity.this, getResources().getString(R.string.address_added_successully));


                if (type.equalsIgnoreCase("save")) {
                    Intent timeAndAddress = new Intent(MapActivity.this, AddressActivity.class);
                    timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(timeAndAddress);

                } else {
                    SelectTimeAndAddressActivity.isTimeSelected = false;
                    SelectTimeAndAddressActivity.isAddressSelected = false;
                    Intent timeAndAddress = new Intent(MapActivity.this, SelectTimeAndAddressActivity.class);
                    timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    timeAndAddress.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(timeAndAddress);
                }

            }
        });
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("address", locationName.getText().toString());
            jsonObject.put("latitude", "" + currentLatitude);
            jsonObject.put("longitude", "" + currentLongitude);
            jsonObject.put("doorno", doorNo.getText().toString());
            jsonObject.put("landmark", landMark.getText().toString());
            jsonObject.put("city", cityName);
            jsonObject.put("title", title.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    private JSONObject getUpdateInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("address", locationName.getText().toString());
            jsonObject.put("latitude", "" + currentLatitude);
            jsonObject.put("longitude", "" + currentLongitude);
            jsonObject.put("doorno", doorNo.getText().toString());
            jsonObject.put("landmark", landMark.getText().toString());
            jsonObject.put("city", cityName);
            jsonObject.put("id", getIntent().getStringExtra("id"));
            jsonObject.put("title", title.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void initViews() {

        editAddress = (LinearLayout) findViewById(R.id.editAddress);
        confirmButton = (Button) findViewById(R.id.confirmButton);
        Utils.setButtonColor(MapActivity.this, confirmButton);
        backButton = (ImageView) findViewById(R.id.backButton);
        markerIcon = (ImageView) findViewById(R.id.markerIcon);
        myLocation = (ImageView) findViewById(R.id.myLocation);
        Utils.setIconColour(MapActivity.this, myLocation.getDrawable());
        locationName = (EditText) findViewById(R.id.locationName);
        title = (EditText) findViewById(R.id.title);
        doorNo = (EditText) findViewById(R.id.doorNo);
        landMark = (EditText) findViewById(R.id.landMark);
        editLayout = (LinearLayout) findViewById(R.id.editLayout);
        mapParent = (RelativeLayout) findViewById(R.id.mapParent);
        addAddressButton = (LinearLayout) findViewById(R.id.addAddressButton);
        editLayout.setVisibility(View.GONE);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        addAddressButton.setVisibility(View.VISIBLE);

        editLayout.setVisibility(View.GONE);
        type = getIntent().getStringExtra("from");

        if (type.equalsIgnoreCase("edit")) {
            slideUp();
            title.setText(getIntent().getStringExtra("title"));
            doorNo.setText(getIntent().getStringExtra("doorNo"));
            landMark.setText(getIntent().getStringExtra("landMark"));
            confirmButton.setText(getResources().getString(R.string.save));
        }

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkGpsisEnabled(MapActivity.this)) {
                    LatLng myLocation;

                    myLocation = new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude());

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(18).build();
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } else {
                    settingsrequest();
                    initlocationRequest();
                }
            }
        });

    }

//    public boolean checkGpsisEnabled() {
//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setBuildingsEnabled(true);
        map.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                locationName.setText("");
                locationName.setHint(getResources().getString(R.string.fetching));

            }
        });
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Utils.log("onCameraIdle: ", "idle_values:" + map.getCameraPosition().target);
                ApiCall.getCompleteAddressString(MapActivity.this, map.getCameraPosition().target.latitude, map.getCameraPosition().target.longitude, new Callback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        currentLatitude = map.getCameraPosition().target.latitude;
                        currentLongitude = map.getCameraPosition().target.longitude;

                        JSONArray Results = null;
                        try {
                            Results = result.getJSONArray("results");
                            JSONObject zero = Results.getJSONObject(0);
                            JSONArray address_components = zero
                                    .getJSONArray("address_components");

                            for (int i = 0; i < address_components.length(); i++) {
                                JSONObject zero2 = address_components
                                        .getJSONObject(i);
                                String long_name = zero2.getString("long_name");
                                JSONArray mtypes = zero2.getJSONArray("types");
                                String Type = mtypes.getString(0);
                                if (Type.equalsIgnoreCase("administrative_area_level_2")) {
                                    cityName = long_name;
                                }

                            }

                            String strAdd = ((JSONArray) result.get("results")).getJSONObject(0).getString("formatted_address");
                            locationName.setText(strAdd);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

            }
        });

        map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                if (value == 0) {
                    LatLng myLocation;

                    myLocation = new LatLng(location.getLatitude(), location.getLongitude());

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(18).build();
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    map.setPadding(0, 0, 0, 0);
                    map.getUiSettings().setZoomControlsEnabled(false);
                    map.getUiSettings().setMapToolbarEnabled(false);
                    map.getUiSettings().setCompassEnabled(false);
                    value++;

                }

            }
        });

        if (type.equalsIgnoreCase("edit")) {
            LatLng myLocation;
            currentLatitude = Double.parseDouble(getIntent().getStringExtra("latitude"));
            currentLongitude = Double.parseDouble(getIntent().getStringExtra("longitude"));
            myLocation = new LatLng(currentLatitude, currentLongitude);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(18).build();
            map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            map.setPadding(0, 0, 0, 0);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.getUiSettings().setMapToolbarEnabled(false);
            map.getUiSettings().setCompassEnabled(false);
            value++;
        }

        map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.map));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onLocationChanged(Location location) {
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
                        location.getLongitude()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.moveCamera(center);
        map.animateCamera(zoom);

        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {

                initlocationRequest();
            }
        }

        if (requestCode == 1) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initlocationRequest();
                settingsrequest();

            }

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {

            }
        } else {

            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            Utils.log(TAG, "mylat:" + currentLatitude + ",mylong:" + currentLongitude);
            if (currentLatitude != 0.0 && currentLongitude != 0.0) {

//                    setlocation();

            }

        }
    }

//    private void setlocation() {
//        String url = "http://maps.googleapis.com/maps/api/geocode/json?key="+getResources().getString(R.string.google_maps_key)+"&latlng=" + currentLatitude + "," + currentLongitude + "&sensor=true";
//        Utils.log(TAG, url);
//        ApiCall.getMethod(MapActivity.this,url, new VolleyCallback() {
//            @Override
//            public void onSuccess(JSONObject response) {
//                try {
//                    JSONArray jsonArray = new JSONArray();
//                    jsonArray = response.optJSONArray("results");
//                    locationName.setText(jsonArray.optJSONObject(0).optString("formatted_address"));
//                } catch (Exception e) {
//
//                }
//            }
//
//        });
//    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

}
