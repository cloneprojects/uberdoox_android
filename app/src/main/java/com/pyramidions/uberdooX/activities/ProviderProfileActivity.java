package com.pyramidions.uberdooX.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.adapters.OtherServicesAdapter;
import com.pyramidions.uberdooX.adapters.ReviewsAdapter;
import com.pyramidions.uberdooX.fragments.ProfileFragment;
import com.pyramidions.uberdooX.fragments.ReviewsFragment;
import com.pyramidions.uberdooX.helpers.MyLinearLayoutManager;
import com.pyramidions.uberdooX.helpers.OnSwipeListener;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProviderProfileActivity extends AppCompatActivity implements ProfileFragment.OnFragmentInteractionListener, ReviewsFragment.OnFragmentInteractionListener {

    JSONObject jsonObject = new JSONObject();
    JSONArray reviewsArray = new JSONArray();
    JSONArray otherServices = new JSONArray();
    LinearLayout reviewParent;
    //    private GestureDetectorCompat detector;
//    private NavigationTabStrip tabStrip;
//    private View swipeView;
//    private ViewPager providerPager;
    ImageView providerPic;
    private TextView providerName, providerMobile;
    TextView readMore;
    private Button bookNow;
    private ImageView backButton;
    ScrollView scrollView;
    RecyclerView reviewsRecyclerView, reviewsRecyclerView_one;
    TextView summaryContent, distance, rating;
    TextView priceContent;
    CardView callButton;
    TextView reviewNameOne, reviewNameTwo, reviewNameThree;
    TextView reviewContentOne, reviewContentTwo, reviewContentThree;
    RatingBar ratingOne, ratingTwo, ratingThree;
    RelativeLayout rateOne, rateTwo, rateThree;
    TextView addressValue;
    AppSettings appSettings = new AppSettings(ProviderProfileActivity.this);
//    private OnSwipeListener onSwipeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ProviderProfileActivity.this, "theme_value");
        Utils.Setheme(ProviderProfileActivity.this, theme_value);
        setContentView(R.layout.activity_provider_profile);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        initViews();
        initListners();
        try {
            setValues();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initAdapters();

    }

    private void initAdapters() {

        reviewsRecyclerView = findViewById(R.id.reviewsRecyclerView);
        reviewsRecyclerView_one = findViewById(R.id.reviewsRecyclerView_one);


        LinearLayoutManager timeLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        OtherServicesAdapter reviewsAdapter = new OtherServicesAdapter(this, otherServices);
        reviewsRecyclerView_one.setLayoutManager(timeLayoutManager);
        reviewsRecyclerView_one.setAdapter(reviewsAdapter);


//        scrollView.fullScroll(View.FOCUS_UP);
    }


    private void initViews() {
//        tabStrip = (NavigationTabStrip) findViewById(R.id.tabStrip);
//        swipeView = (View) findViewById(R.id.swipeView);
        bookNow = (Button) findViewById(R.id.bookNow);
        Utils.setButtonColor(ProviderProfileActivity.this, bookNow);
//        providerPager = (ViewPager) findViewById(R.id.providerPager);
        providerName = (TextView) findViewById(R.id.providerName);
        rating = (TextView) findViewById(R.id.rating);
        distance = (TextView) findViewById(R.id.distance);
        providerMobile = (TextView) findViewById(R.id.providerMobile);
        backButton = (ImageView) findViewById(R.id.backButton);
        providerPic = (ImageView) findViewById(R.id.providerPic);

        Utils.setDrawabe(ProviderProfileActivity.this, providerPic);
//        String[] titles = new String[]{"Profile", "Reviews"};
//        tabStrip.setTitles(titles);


        summaryContent = (TextView) findViewById(R.id.summaryContent);
        priceContent = (TextView) findViewById(R.id.priceContent);

        readMore = (TextView) findViewById(R.id.readMore);
        Utils.setTextColour(ProviderProfileActivity.this, readMore);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        callButton = (CardView) findViewById(R.id.callButton);


        reviewNameOne = (TextView) findViewById(R.id.reviewNameOne);
        reviewNameTwo = (TextView) findViewById(R.id.reviewNameTwo);
        reviewNameThree = (TextView) findViewById(R.id.reviewNameThree);

        reviewContentOne = (TextView) findViewById(R.id.reviewContentOne);
        reviewContentTwo = (TextView) findViewById(R.id.reviewContentTwo);
        reviewContentThree = (TextView) findViewById(R.id.reviewContentThree);

        addressValue = (TextView) findViewById(R.id.addressValue);

        ratingOne = (RatingBar) findViewById(R.id.ratingBarOne);
        ratingTwo = (RatingBar) findViewById(R.id.ratingBarTwo);
        ratingThree = (RatingBar) findViewById(R.id.ratingBarThree);


        rateOne = (RelativeLayout) findViewById(R.id.rateOne);
        rateTwo = (RelativeLayout) findViewById(R.id.rateTwo);
        rateThree = (RelativeLayout) findViewById(R.id.rateThree);


        reviewParent = (LinearLayout) findViewById(R.id.reviewParent);


    }

    private void setValues() throws JSONException {

        jsonObject = new JSONObject(getIntent().getStringExtra("providerDetails"));
        reviewsArray = jsonObject.optJSONArray("reviews");
        otherServices = jsonObject.optJSONArray("provider_services");
        providerName.setText(jsonObject.optString("name"));
        providerName.setText(jsonObject.optString("name"));


        Glide.with(this).load(jsonObject.optString("image"))
               .into(providerPic);

        summaryContent.setText(jsonObject.optString("about"));
        rating.setText(jsonObject.optString("avg_rating"));
        distance.setText(jsonObject.optString("distance") + " km");
        providerMobile.setText(jsonObject.optString("mobile"));
        priceContent.setText("$" + jsonObject.optString("priceperhour") + " " + getResources().getString(R.string.price_per_hour));

        addressValue.setText(jsonObject.optString("addressline1") + " " + jsonObject.optString("addressline2") + " " + jsonObject.optString("city") + " " + jsonObject.optString("state") + " " + jsonObject.optString("zipcode"));
        if (reviewsArray.length() > 3) {
            readMore.setVisibility(View.VISIBLE);
        } else {
            readMore.setVisibility(View.GONE);

        }


        if (reviewsArray.length() == 0) {
            reviewParent.setVisibility(View.GONE);
            rateOne.setVisibility(View.GONE);
            rateTwo.setVisibility(View.GONE);
            rateThree.setVisibility(View.GONE);
        } else if (reviewsArray.length() == 1) {
            reviewParent.setVisibility(View.VISIBLE);

            rateOne.setVisibility(View.VISIBLE);
            rateTwo.setVisibility(View.GONE);
            rateThree.setVisibility(View.GONE);
        } else if (reviewsArray.length() == 2) {
            reviewParent.setVisibility(View.VISIBLE);

            rateOne.setVisibility(View.VISIBLE);
            rateTwo.setVisibility(View.VISIBLE);
            rateThree.setVisibility(View.GONE);
        } else if (reviewsArray.length() == 3) {
            reviewParent.setVisibility(View.VISIBLE);
            rateOne.setVisibility(View.VISIBLE);
            rateTwo.setVisibility(View.VISIBLE);
            rateThree.setVisibility(View.VISIBLE);
        }

        setReviewValues();

//        ProviderPagerAdapter providerPagerAdapter = new ProviderPagerAdapter(getSupportFragmentManager());
//        providerPager.setAdapter(providerPagerAdapter);
//        tabStrip.setViewPager(providerPager);
    }

    private void setReviewValues() {

        for (int i = 0; i < reviewsArray.length(); i++) {
            JSONObject jsonObject = reviewsArray.optJSONObject(i);
            if (i == 0) {
                reviewNameOne.setText(jsonObject.optString("username"));
                reviewContentOne.setText(jsonObject.optString("feedback"));
                ratingOne.setRating(Float.parseFloat(jsonObject.optString("rating")));
            } else if (i == 1) {
                reviewNameTwo.setText(jsonObject.optString("username"));
                reviewContentTwo.setText(jsonObject.optString("feedback"));
                ratingTwo.setRating(Float.parseFloat(jsonObject.optString("rating")));

            } else if (i == 2) {
                reviewNameThree.setText(jsonObject.optString("username"));
                reviewContentThree.setText(jsonObject.optString("feedback"));
                ratingThree.setRating(Float.parseFloat(jsonObject.optString("rating")));

            } else {
                break;
            }
        }
    }

    private void initListners() {

//        swipeView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                Log.d("Touch", motionEvent.toString());
//                return detector.onTouchEvent(motionEvent);
//            }
//        });
//        onSwipeListener = new OnSwipeListener() {
//
//            @Override
//            public boolean onSwipe(Direction direction) {
//
//                Log.d("Swipe", direction.toString());
//                // Possible implementation
//                if (direction == Direction.left || direction == Direction.right) {
//                    // Do something COOL like animation or whatever you want
//                    // Refer to your view if needed using a global reference
//                    return true;
//                } else if (direction == Direction.up || direction == Direction.down) {
//                    // Do something COOL like animation or whatever you want
//                    // Refer to your view if needed using a global reference
//                    if (direction == Direction.down) {
//                        ActivityCompat.finishAfterTransition(ProviderProfileActivity.this);
//                    }
//                    return true;
//                }
//
//                return super.onSwipe(direction);
//            }
//        };
//
//        detector = new GestureDetectorCompat(this, onSwipeListener);

        bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent confirmBooking = new Intent(ProviderProfileActivity.this, ConfirmBookingActivity.class);
                confirmBooking.putExtra("providerName", providerName.getText().toString());
                confirmBooking.putExtra("providerRating", jsonObject.optString("avg_rating"));
                confirmBooking.putExtra("providerService", jsonObject.optString("avg_rating"));
                confirmBooking.putExtra("providerMobile", jsonObject.optString("mobile"));
                confirmBooking.putExtra("selected_id", jsonObject.optString("id"));
                confirmBooking.putExtra("providerPic", jsonObject.optString("image"));
                confirmBooking.putExtra("latitude", jsonObject.optString("latitude"));
                confirmBooking.putExtra("longitude", jsonObject.optString("longitude"));
                String priice = "$" + jsonObject.optString("priceperhour");
                confirmBooking.putExtra("pricing", priice);
                startActivity(confirmBooking);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRatingACtivity();
            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPermissionGranted()) {
                    call_action(jsonObject.optString("mobile"));
                }
            }
        });

    }


    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call_action(jsonObject.optString("mobile"));
                } else {
                    Toast.makeText(getApplicationContext(), R.string.permission_denied, Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void call_action(String phnum) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phnum));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    private void showRatingACtivity() {
        Intent intent = new Intent(ProviderProfileActivity.this, RatingActivity.class);
        intent.putExtra("reviews", reviewsArray.toString());
        startActivity(intent);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAfterTransition(this);
    }

//    public class ProviderPagerAdapter extends FragmentStatePagerAdapter {
//
//
//        public ProviderPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            switch (position) {
//                case 0:
//                    return ProfileFragment.newInstance("Profile", jsonObject.toString());
//                case 1:
//                    return ReviewsFragment.newInstance("Reviews", reviewsArray.toString());
//            }
//            return null;
//        }
//
//        @Override
//        public int getCount() {
//            return 2;
//        }
//    }


}

