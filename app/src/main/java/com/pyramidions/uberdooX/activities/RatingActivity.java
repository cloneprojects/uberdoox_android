package com.pyramidions.uberdooX.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.adapters.ReviewsAdapter;
import com.pyramidions.uberdooX.fragments.ReviewsFragment;

import org.json.JSONArray;
import org.json.JSONException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RatingActivity extends AppCompatActivity {
    RecyclerView ratingRecyclerView;
    ImageView backButton;
    private JSONArray reviews = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        ratingRecyclerView = (RecyclerView) findViewById(R.id.ratingRecyclerView);
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        try {
            reviews = new JSONArray(getIntent().getStringExtra("reviews"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LinearLayoutManager timeLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        ratingRecyclerView.setLayoutManager(timeLayoutManager);
        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(this, reviews);
        ratingRecyclerView.setAdapter(reviewsAdapter);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
