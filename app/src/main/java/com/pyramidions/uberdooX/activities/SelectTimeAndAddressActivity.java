package com.pyramidions.uberdooX.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.util.Util;
import com.pyramidions.uberdooX.Events.Status;
import com.pyramidions.uberdooX.Events.onSelected;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.adapters.AddressAdapter;
import com.pyramidions.uberdooX.adapters.DatesAdapter;
import com.pyramidions.uberdooX.adapters.TimesAdapter;
import com.pyramidions.uberdooX.helpers.CircularTextView;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SelectTimeAndAddressActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView datesRecyclerView;
    RecyclerView addressRecyclerView;
    RecyclerView timesRecyclerView;
    Button next;
    TextView emptyLayout;
    public static boolean isTodaySelected = true;
    RelativeLayout guest_login;
    public static Boolean isTimeSelected = false, isDateSeleceted = false, isAddressSelected = false;
    ImageView backButton;
    private JSONArray addressArray = new JSONArray();
    private JSONArray timesArray = new JSONArray();
    private String TAG = SelectTimeAndAddressActivity.class.getSimpleName();
    AppSettings appSettings = new AppSettings(SelectTimeAndAddressActivity.this);
    private TimesAdapter timesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(SelectTimeAndAddressActivity.this, "theme_value");
        Utils.Setheme(SelectTimeAndAddressActivity.this, theme_value);
        setContentView(R.layout.activity_select_time_and_address);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        isDateSeleceted = false;
        isAddressSelected = false;
        isTodaySelected = true;
        try {
            initViews();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectedEvent(onSelected event) throws ParseException {
//        if (event.getCheckValue().equalsIgnoreCase("no")) {
        timesArray = new JSONArray();
        timesArray = appSettings.getTimeSlots();
        Log.d(TAG, "timesArray: " + timesArray);

        for (int i = timesArray.length() - 1; i >= 0; i--) {
            if (isTodaySelected) {
                String fromTime = timesArray.optJSONObject(i).optString("toTime");
                Date date = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).parse(fromTime);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
                Date currentdate = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).parse(currentDateandTime);
                long currenttime = currentdate.getTime();
                long serviceTime = date.getTime();
                Log.d(TAG, "serviceTIme: " + currenttime + "/" + serviceTime);

                int from = Integer.parseInt(timesArray.optJSONObject(i).optString("fromTime").substring(0, 2));
                int to = Integer.parseInt(timesArray.optJSONObject(i).optString("toTime").substring(0, 2));
                Log.d(TAG, "onSelectedEvent: " + from + "/" + to);

                if (from < to) {
                } else {
                    serviceTime = serviceTime + 86400000;
                }

                if (serviceTime < currenttime) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        timesArray.remove(i);
                    }
                }
            } else {
                timesArray.optJSONObject(i).remove("selected");
                try {
                    timesArray.optJSONObject(i).put("selected", "false");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        Log.d(TAG, "timesArrayafter: " + timesArray);

        GridLayoutManager timeLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);

        timesRecyclerView.setLayoutManager(timeLayoutManager);
        timesAdapter = new TimesAdapter(this, timesArray);
        timesRecyclerView.setAdapter(timesAdapter);
        //timesAdapter.notifyDataSetChanged();

        if (timesArray.length() == 0) {
            showEmptyLayout();
        } else {
            showValueLayout();
        }


    }

    private void showValueLayout() {

        emptyLayout.setVisibility(View.INVISIBLE);
        timesRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showEmptyLayout() {
        emptyLayout.setVisibility(View.VISIBLE);
        timesRecyclerView.setVisibility(View.INVISIBLE);
    }


    private void initListners() {
        next.setOnClickListener(this);

        backButton.setOnClickListener(this);
    }

    private void setTimeSLotsAdapter() throws ParseException {
        isTodaySelected = true;
        AppSettings appSettings = new AppSettings(SelectTimeAndAddressActivity.this);
        timesArray = appSettings.getTimeSlots();
        //
        //
        // Log.d(TAG, "setTimeSLotsAdapter: " + timesArray);


        for (int i = timesArray.length() - 1; i >= 0; i--) {
            String fromTime = timesArray.optJSONObject(i).optString("toTime");
            Date date = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).parse(fromTime);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
            Date currentdate = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).parse(currentDateandTime);
            long currenttime = currentdate.getTime();
            long serviceTime = date.getTime();


            int from = Integer.parseInt(timesArray.optJSONObject(i).optString("fromTime").substring(0, 2));
            int to = Integer.parseInt(timesArray.optJSONObject(i).optString("toTime").substring(0, 2));
            Log.d(TAG, "onSelectedEvent: " + from + "/" + to);
            if (from < to) {
            } else {
                serviceTime = serviceTime + 86400000;
            }


            Log.d(TAG, "serviceTIme: " + currenttime + "/" + serviceTime);
            if (serviceTime < currenttime) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    timesArray.remove(i);
                }
            }
        }


        //  Log.d(TAG, "remaingArray: " + timesArray);


        GridLayoutManager timeLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);

        timesRecyclerView.setLayoutManager(timeLayoutManager);
        timesAdapter = new TimesAdapter(this, timesArray);
        timesRecyclerView.setAdapter(timesAdapter);

        if (timesArray.length() == 0) {
            showEmptyLayout();
        } else {
            showValueLayout();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void moveShowProviderList() {
        Intent providerProfile = new Intent(SelectTimeAndAddressActivity.this, ShowProvidersActivity.class);
        startActivity(providerProfile);
    }

    private void SetAddressAdapter() {

        ApiCall.getMethodHeaders(SelectTimeAndAddressActivity.this, UrlHelper.LIST_ADDRESS, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.log(TAG, "response:" + response);
                addressArray = response.optJSONArray("list_address");

                for (int i = 0; i < addressArray.length(); i++) {

                    JSONObject jsonObject = addressArray.optJSONObject(i);
                    try {
                        jsonObject.put("selected", "false");
                        addressArray.put(i, jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                JSONObject object = new JSONObject();
                try {
                    object.put("type", "addaddress");
                    addressArray.put(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GridLayoutManager addressLayoutManager = new GridLayoutManager(SelectTimeAndAddressActivity.this, 1, GridLayoutManager.HORIZONTAL, false);
                addressRecyclerView.setLayoutManager(addressLayoutManager);
                AddressAdapter addressAdapter = new AddressAdapter(SelectTimeAndAddressActivity.this, addressArray);
                addressRecyclerView.setAdapter(addressAdapter);
            }
        });


    }

    private void setDatesAdapter() {


        SimpleDateFormat format = new SimpleDateFormat("dd", Locale.getDefault());
        SimpleDateFormat ffformat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar date = Calendar.getInstance();
        JSONArray datesArray = new JSONArray();

        for (int i = 0; i < 7; i++) {
            JSONObject day = new JSONObject();
            try {
                day.put("date", format.format(date.getTime()));
                day.put("fullDate", ffformat.format(date.getTime()));
                day.put("day", new SimpleDateFormat("EE",
                        Locale.getDefault()).format(date.getTime()).toUpperCase());
                if (i == 0) {
                    day.put("selected", "true");
                } else {
                    day.put("selected", "false");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            datesArray.put(day);
            date.add(Calendar.DATE, 1);
        }

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        datesRecyclerView.setLayoutManager(manager);
        DatesAdapter datesAdapter = new DatesAdapter(SelectTimeAndAddressActivity.this, datesArray);
        datesRecyclerView.setAdapter(datesAdapter);

    }

    private void initViews() throws ParseException {

        datesRecyclerView = (RecyclerView) findViewById(R.id.datesRecyclerView);
        addressRecyclerView = findViewById(R.id.addressRecyclerView);
        backButton = (ImageView) findViewById(R.id.backButton);
        emptyLayout = (TextView) findViewById(R.id.emptyLayout);
        next = (Button) findViewById(R.id.next);
        Utils.setButtonColor(SelectTimeAndAddressActivity.this, next);
        timesRecyclerView = (RecyclerView) findViewById(R.id.timeRecyclerView);
        guest_login = (RelativeLayout) findViewById(R.id.guest_login);

        if (appSettings.getUserType().equalsIgnoreCase("guest")) {
            guest_login.setVisibility(View.VISIBLE);
        } else {
            guest_login.setVisibility(View.GONE);
            initListners();
            setDatesAdapter();
            setTimeSLotsAdapter();
            SetAddressAdapter();

        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isAddressSelected = false;
        isDateSeleceted = false;
        isTimeSelected = false;
    }

    @Override
    public void onClick(View view) {
        if (view == next) {
            if (isAddressSelected) {
                if (isDateSeleceted) {
                    if (isTimeSelected) {
                        moveShowProviderList();
                    } else {
                        Utils.toast(SelectTimeAndAddressActivity.this, getResources().getString(R.string.please_select_time));
                    }
                } else {
                    Utils.toast(SelectTimeAndAddressActivity.this, getResources().getString(R.string.please_select_date));

                }
            } else {
                Utils.toast(SelectTimeAndAddressActivity.this, getResources().getString(R.string.please_select_address));

            }

        } else if (view == backButton) {
            isAddressSelected = false;
            isDateSeleceted = false;
            isTimeSelected = false;
            onBackPressed();
            finish();
        }

    }


}
