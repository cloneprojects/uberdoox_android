package com.pyramidions.uberdooX.activities;

import android.Manifest;
import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.adapters.ProvidersAdapter;
import com.pyramidions.uberdooX.adapters.ProvidersListAdapter;
import com.pyramidions.uberdooX.helpers.MapRipple;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ShowProvidersActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    DiscreteScrollView providersRecyclerView;
    GoogleMap map;
    AppSettings appSettings = new AppSettings(ShowProvidersActivity.this);
    ImageView sortProvider;
    ImageView arrowIcon;
    private android.location.LocationManager locationManager;
    private JSONArray providers = new JSONArray();
    private int val = 0;
    FrameLayout bottomSheet;
    private TextView nameView;
    Boolean isSheetExpanded = false;
    private TextView distance;
    private RatingBar providerRating;
    private int selectedposition = 0;
    private SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private ImageView backButton;
    String selected_lat, selected_lng;
    RecyclerView providerList;
    private ValueAnimator vAnimator;
    private MapRipple mapRipple;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ShowProvidersActivity.this, "theme_value");
        Utils.Setheme(ShowProvidersActivity.this, theme_value);
        setContentView(R.layout.activity_show_providers);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        initViews();
        getDatas();
        initlocation();
        initListners();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            settingsrequest();
            initlocationRequest();
        }
    }

    private void initlocation() {
        checkCallingOrSelfPermission("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }


    private void initlocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ShowProvidersActivity.this, 5);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    private void getDatas() {

        ApiCall.PostMethodHeaders(ShowProvidersActivity.this, UrlHelper.LIST_PROVIDERS, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                providers = response.optJSONArray("all_providers");

                double lat = Double.parseDouble(providers.optJSONObject(0).optString("latitude"));
                double lng = Double.parseDouble(providers.optJSONObject(0).optString("longitude"));

                Log.d("onLocationChanged: ", "defal:" + lat + ",long:" + lng);
                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(new LatLng(lat,
                                lng));
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                map.moveCamera(center);
                map.animateCamera(zoom);
                val++;
                mapRipple = new MapRipple(map, new LatLng(lat,
                        lng), ShowProvidersActivity.this);

                for (int i = 0; i < providers.length(); i++) {
                    createMarker(i);

                }


                final ProvidersAdapter adapter = new ProvidersAdapter(ShowProvidersActivity.this, providers);
                providersRecyclerView.setAdapter(adapter);

                final ProvidersListAdapter adapters = new ProvidersListAdapter(ShowProvidersActivity.this, providers);
                LinearLayoutManager verticalmanager = new LinearLayoutManager(ShowProvidersActivity.this, LinearLayoutManager.VERTICAL, false);
                providerList.setLayoutManager(verticalmanager);
                providerList.setAdapter(adapters);

            }
        });

    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("service_sub_category_id", appSettings.getSelectedSubCategory());
            jsonObject.put("time_slot_id", appSettings.getSelectedTimeSlot());
            jsonObject.put("date", appSettings.getSelectedDate());
            jsonObject.put("city", appSettings.getSelectedCity());
            jsonObject.put("lat", appSettings.getSelectedlat());
            jsonObject.put("lon", appSettings.getSelectedLong());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public Bitmap getBitmap() {
        Drawable drawable = getResources().getDrawable(R.drawable.circle_violet);
        drawable.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(ShowProvidersActivity.this),
                PorterDuff.Mode.SRC_IN));
        try {
            Bitmap bitmap;

            bitmap = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (OutOfMemoryError e) {
            // Handle the error
            return null;
        }
    }


    public void createRippleMarker(final LatLng latLng) {

        try {
            mapRipple.stopRippleMapAnimation();
        } catch (Exception e) {


        }
        mapRipple.withLatLng(latLng);
        mapRipple.withNumberOfRipples(3);
        mapRipple.withFillColor(Utils.getPrimaryCOlor(ShowProvidersActivity.this));
        mapRipple.withStrokeColor(Utils.getPrimaryCOlor(ShowProvidersActivity.this));
        mapRipple.withStrokewidth(10);   // 10dp
        mapRipple.withDistance(10);   // 2000 metres radius
        mapRipple.withRippleDuration(3000);   //12000ms
        mapRipple.withTransparency(0.5f);

        // 10dp
        mapRipple.startRippleMapAnimation();      //in onMapReadyCallBack

    }

    public void createMarker(int position) {
        Bitmap smallMarker = getBitmap();

        MarkerOptions markerOptions = new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

        // Setting position on the MarkerOptions

        double lat = Double.parseDouble(providers.optJSONObject(position).optString("latitude"));
        double lng = Double.parseDouble(providers.optJSONObject(position).optString("longitude"));
        LatLng latLng = new LatLng(lat, lng);
        markerOptions.position(latLng);
        map.addMarker(markerOptions);
    }

    private void initListners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSheetExpanded) {
                    onBackPressed();
                }
            }
        });
        sortProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterPoup();
            }
        });


        providersRecyclerView.addOnItemChangedListener(new DiscreteScrollView.OnItemChangedListener
                <RecyclerView.ViewHolder>() {
            @Override
            public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
                Log.d("ITEM CHANGED ", String.valueOf(adapterPosition));

                selectedposition = adapterPosition;

                // Setting position on the MarkerOptions

                double lat = Double.parseDouble(providers.optJSONObject(adapterPosition).optString("latitude"));
                double lng = Double.parseDouble(providers.optJSONObject(adapterPosition).optString("longitude"));
                LatLng latLng = new LatLng(lat, lng);
                createRippleMarker(latLng);

                // Animating to the currently touched position
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(15).build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                // Adding marker on the GoogleMap


            }
        });

    }

    private void initViews() {
        providersRecyclerView = (DiscreteScrollView) findViewById(R.id.providersRecyclerView);
        providerList = (RecyclerView) findViewById(R.id.providerList);
        sortProvider = (ImageView) findViewById(R.id.sortProvider);
        arrowIcon = (ImageView) findViewById(R.id.arrowIcon);


        backButton = (ImageView) findViewById(R.id.backButton);
        bottomSheet = (FrameLayout) findViewById(R.id.bottomSheet);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    isSheetExpanded = true;
                    bottomSheet.setBackgroundColor(getResources().getColor(R.color.white));
                    arrowIcon.setRotation(180);
                }

                if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_DRAGGING) {
                    isSheetExpanded = false;
                    bottomSheet.setBackgroundColor(getResources().getColor(R.color.white));
                }
                if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    isSheetExpanded = false;
                    bottomSheet.setBackgroundColor(getResources().getColor(R.color.transparent));
                    arrowIcon.setRotation(0);

                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });


    }

    public void showFilterPoup() {
        Dialog dialog = new Dialog(ShowProvidersActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.filter_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.clear();
//        map.setInfoWindowAdapter(this);

        map.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (val == 0) {

        }
        map.setMyLocationEnabled(false);

        map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.map));
        map.getUiSettings().setMyLocationButtonEnabled(false);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {

                initlocationRequest();
            }
        }

        if (requestCode == 1) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initlocationRequest();
                settingsrequest();

            }

        }
    }


//    @Override
//    public View getInfoWindow(Marker marker) {
//        return null;
//    }
//
//    @Override
//    public View getInfoContents(Marker marker) {
//        return prepareInfoView(marker);
//    }
//
//
//    private View prepareInfoView(Marker marker) {
//        //prepare InfoView programmatically
//
//        View view = getLayoutInflater().inflate(R.layout.custom_info_layout, null);
//        view.setLayoutParams(new LinearLayoutCompat.LayoutParams(520, 230));
//        nameView = view.findViewById(R.id.providerName);
//        distance = view.findViewById(R.id.distance);
//        providerRating = view.findViewById(R.id.providerRating);
//
//        nameView.setText(providers.optJSONObject(selectedposition).optString("name"));
//        double val = Double.parseDouble(providers.optJSONObject(selectedposition).optString("distance"));
//        String disntac = String.format("%.2f", val);
//        distance.setText(disntac);
//        try {
//            providerRating.setRating(Float.parseFloat(providers.optJSONObject(selectedposition).optString("avg_rating")));
//        } catch (Exception e) {
//
//        }
//        return view;
//    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {

            }
        } else {


        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
