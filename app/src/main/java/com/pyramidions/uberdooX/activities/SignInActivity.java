package com.pyramidions.uberdooX.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.pyramidions.uberdooX.helpers.Utils.filter;
import static com.pyramidions.uberdooX.helpers.Utils.isValidEmail;

/**
 * Created by karthik on 01/10/17.
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    EditText usernameEditText, passwordEditText;
    Button dontHaveAnAccount, loginButton, forgotPassword;
    ImageView facebookLogin, googleLogin;
    LoginButton loginFbButton;
    FrameLayout facebookLoginButton;
    CallbackManager callbackManager;
    AppSettings appSettings = new AppSettings(SignInActivity.this);
    private String TAG = SignInActivity.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 420;

//    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
//        @Override
//        public CharSequence getTransformation(CharSequence source, View view) {
//            return new AsteriskPasswordTransformationMethod.PasswordCharSequence(source);
//        }
//
//        private class PasswordCharSequence implements CharSequence {
//            private CharSequence mSource;
//
//            public PasswordCharSequence(CharSequence source) {
//                mSource = source; // Store char sequence
//            }
//
//            public char charAt(int index) {
//                return '*'; // This is the important part
//            }
//
//            public int length() {
//                return mSource.length(); // Return default
//            }
//
//            public CharSequence subSequence(int start, int end) {
//                return mSource.subSequence(start, end); // Return default
//            }
//        }
//    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(SignInActivity.this, "theme_value");
        Utils.Setheme(SignInActivity.this, theme_value);
        setContentView(R.layout.activity_signin);
        FacebookSdk.sdkInitialize(getApplicationContext());


        initViews();
        initListners();
        initFacebookLogin();
        initGoogleLogin();
        // Utils.show(SignInActivity.this);
    }

    private void initGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void googlesignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginFbButton = (LoginButton) findViewById(R.id.loginFbButton);

        loginFbButton.setReadPermissions("email");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                String accessToken = loginResult.getAccessToken().getToken();
                Log.e(TAG, "Access token: " + accessToken);
                get_data_for_facebook(loginResult);
            }

            @Override
            public void onCancel() {
                // App code
                Log.e(TAG, "onCancel: " + "canceling the facebook");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e(TAG, "onError: " + exception);
            }
        });

    }

    private void get_data_for_facebook(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e(TAG, "Facebook_Response :" + object);
                        final String email_json = object.optString("email");
                        final String first_name = object.optString("first_name");
                        final String last_name = object.optString("last_name");

                        Log.e(TAG, "facebook email: " + email_json);
                        final String id = object.optString("id");
                        Log.e(TAG, "facebook id: " + id);
                        String type = "facebook";
                        String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
                        if (email_json.equals("")) {
                            LoginManager.getInstance().logOut();
                            AlertDialog.Builder alert = new AlertDialog.Builder(SignInActivity.this);

                            alert.setMessage(getResources().getString(R.string.no_email_error_fb));
                            alert.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alert.show();
                        } else {
                            socialLogin(email_json, first_name, last_name, id, type, imageUrl);
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, locale ");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void socialLogin(String email_json, String first_name, String last_name,
                             String id, String type, final String imageUrl) {

        Log.e("google", "infoSuccess:");
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("email", email_json);
            jsonObject.put("firstname", first_name);
            jsonObject.put("lastname", last_name);
            jsonObject.put("socialtoken", id);
            jsonObject.put("social_type", type);
            if (imageUrl.equalsIgnoreCase("null")) {
                jsonObject.put("image", " ");
            } else {
                jsonObject.put("image", imageUrl);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiCall.PostMethod(SignInActivity.this, UrlHelper.SOCIAL_LOGIN, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.e("google", "apiSuccess:");
                appSettings.setToken(response.optString("access_token"));
                appSettings.setIsLogged("true");
                appSettings.setIsSocialLogin("true");
                appSettings.setUserImage(imageUrl);
                moveMainActivity();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Utils.log("google", "Googleresponse" + data);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }

    @Override
    public void onStart() {
        super.onStart();

//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
//        if (opr.isDone()) {
//            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
//            // and the GoogleSignInResult will be available instantly.
//            Log.d(TAG, "Got cached sign-in");
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
//        } else {
//            // If the user has not previously signed in on this device or the sign-in has expired,
//            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
//            // single sign-on will occur in this branch.
//            Utils.show(SignInActivity.this);
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
//                    Utils.dismiss();
//                    handleSignInResult(googleSignInResult);
//                }
//            });
//        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Utils.log("google", "Googleresponse" + result.getStatus());

        if (result.isSuccess()) {
            GoogleSignInAccount googleSignInAccount = result.getSignInAccount();
            String email = googleSignInAccount.getEmail();
            String first_name, last_name;
            String type = "google";
            String id = googleSignInAccount.getId();
            String imageUrl = String.valueOf(googleSignInAccount.getPhotoUrl());
            String full_name = googleSignInAccount.getDisplayName();
            if (full_name.contains(" ")) {
                String[] names = full_name.split(" ");
                first_name = names[0];
                last_name = names[1];
            } else {
                first_name = full_name;
                last_name = "";
            }
            socialLogin(email, first_name, last_name, id, type, imageUrl);
        }
    }

    private void initViews() {
        dontHaveAnAccount = (Button) findViewById(R.id.dontHaveAnAccount);
        facebookLoginButton = (FrameLayout) findViewById(R.id.facebookLoginButton);
        loginButton = (Button) findViewById(R.id.loginButton);
        Utils.setButtonColor(SignInActivity.this, loginButton);
        forgotPassword = (Button) findViewById(R.id.forgotPassword);
        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        facebookLogin = (ImageView) findViewById(R.id.facebookLogin);
        googleLogin = (ImageView) findViewById(R.id.googleLogin);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
    }

//    InputFilter filter = new InputFilter() {
//        public CharSequence filter(CharSequence source, int start, int end,
//                                   Spanned dest, int dstart, int dend) {
//            for (int i = start; i < end; i++) {
//                if (Character.isWhitespace(source.charAt(i))) {
//                    return "";
//                }
//            }
//            return null;
//        }
//
//    };

    private void initListners() {
        dontHaveAnAccount.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        facebookLogin.setOnClickListener(this);
        googleLogin.setOnClickListener(this);
        passwordEditText.setFilters(new InputFilter[]{filter});
        passwordEditText.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        if (view == dontHaveAnAccount) {
            moveSignUp();
        } else if (view == loginButton) {
            if (isValidInputs().equalsIgnoreCase("true")) {
                processLogin();
            } else {
                Utils.toast(SignInActivity.this, isValidInputs());
            }

        } else if (view == forgotPassword) {
            moveToForgotPassword();
        } else if (view == facebookLogin) {
            loginFbButton.performClick();
        } else if (view == googleLogin) {
            googlesignIn();
        }
    }

    private void moveToForgotPassword() {
        Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

//    public final static boolean isValidEmail(CharSequence target) {
//        if (target == null) {
//            return false;
//        } else {
//            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
//        }
//    }

    private String isValidInputs() {
        String value;
        if (usernameEditText.getText().toString().trim().length() == 0 || !isValidEmail(usernameEditText.getText().toString().trim())) {
            value = getResources().getString(R.string.please_enter_valid_email);
        } else if (passwordEditText.getText().length() == 0) {
            value = getResources().getString(R.string.please_enter_valid_password);
        } else {
            value = "true";
        }
        return value;
    }

    private void processLogin() {
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.SIGN_IN, getSignInInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.log("Image", "resp: " + response);
                appSettings.setToken(response.optString("access_token"));
                appSettings.setIsLogged("true");
                appSettings.setIsSocialLogin("false");
                appSettings.setUserImage(response.optString("image"));

                appSettings.setUserName(response.optString("first_name") + " " + response.optString("last_name"));
                appSettings.setUserNumber(response.optString("mobile"));

                moveMainActivity();
//                moveLocationSelectionActivity();
            }


        });
    }

    private void moveLocationSelectionActivity() {
        appSettings.setUserType("guest");

        Intent main = new Intent(SignInActivity.this, SelectLocationActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(main);
        finish();

    }

    private JSONObject getSignInInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", usernameEditText.getText().toString().trim());
            jsonObject.put("password", passwordEditText.getText().toString());
            jsonObject.put("user_type", "User");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;

    }

    private void moveMainActivity() {
        appSettings.setUserType("user");
        Intent main = new Intent(SignInActivity.this, MainActivity.class);
        main.putExtra("type", "new");
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(main);
        finish();
    }


    public void moveSignUp() {
        Intent signup = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivity(signup);
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
