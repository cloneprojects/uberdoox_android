package com.pyramidions.uberdooX.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.BuildConfig;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.pyramidions.uberdooX.helpers.Utils.filter;
import static com.pyramidions.uberdooX.helpers.Utils.isValidEmail;

/**
 * Created by karthik on 01/10/17.
 */
public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    EditText firstNameEditText, lastNameEditText, emailEditText, phoneEditText, passwordEditText;
    Button signUpButton, alreadyHaveAnAccount;
    CircleImageView profilePic;
    File uploadFile;
    private Uri uri;
    private Uri uriCamera;
    private final int cameraIntent = 2;
    private final int galleryIntent = 1;
    private String TAG = SignUpActivity.class.getSimpleName();
    private String uploadImagepath = "";
    AppSettings appSettings;

//    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
//        @Override
//        public CharSequence getTransformation(CharSequence source, View view) {
//            return new AsteriskPasswordTransformationMethod.PasswordCharSequence(source);
//        }
//
//        private class PasswordCharSequence implements CharSequence {
//            private CharSequence mSource;
//
//            public PasswordCharSequence(CharSequence source) {
//                mSource = source; // Store char sequence
//            }
//
//            public char charAt(int index) {
//                return '*'; // This is the important part
//            }
//
//            public int length() {
//                return mSource.length(); // Return default
//            }
//
//            public CharSequence subSequence(int start, int end) {
//                return mSource.subSequence(start, end); // Return default
//            }
//        }
//    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(SignUpActivity.this, "theme_value");
        Utils.Setheme(SignUpActivity.this, theme_value);
        setContentView(R.layout.activity_signup);
        initViews();
        initListners();

    }


    private void initViews() {
        appSettings = new AppSettings(SignUpActivity.this);

        alreadyHaveAnAccount = (Button) findViewById(R.id.alreadyHaveAnAccount);
        signUpButton = (Button) findViewById(R.id.signUpButton);
        Utils.setButtonColor(SignUpActivity.this, signUpButton);
        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        phoneEditText = (EditText) findViewById(R.id.phoneEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        profilePic = findViewById(R.id.profilePic);
       /* Drawable drawable = getResources().getDrawable(R.drawable.dp);
        drawable.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(SignUpActivity.this),
                PorterDuff.Mode.SRC_IN));*/
        ColorFilter colorFilter = new PorterDuffColorFilter(Utils.getPrimaryCOlor(SignUpActivity.this),
                PorterDuff.Mode.SRC_IN);
        profilePic.setColorFilter(colorFilter);

        passwordEditText.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
        passwordEditText.setFilters(new InputFilter[]{filter});

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

    }

    private void initListners() {
        alreadyHaveAnAccount.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        profilePic.setOnClickListener(this);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {

        if (view == alreadyHaveAnAccount) {
            moveToSignIn();
        } else if (view == signUpButton) {
//            proceedSignUp();
            if (validInputs().equalsIgnoreCase("true")) {
                if (uploadFile != null) {

                    uploadImage();
                } else {
                    Utils.show(SignUpActivity.this);
                    proceedSignUp();

                }
            } else {
                Utils.toast(SignUpActivity.this, validInputs());
            }
        } else if (view == profilePic) {
            showPictureDialog();
        }
    }


    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, galleryIntent);

    }

    private void takePhotoFromCamera() {
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(),
                getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder,
                getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }

        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

//        uri = Uri.fromFile(file);

//        uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(SignUpActivity.this, getPackageName() + ".provider", file);
            Log.d(TAG, "onClick: " + uri.getPath());
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        } else {
            uri = Uri.fromFile(file);
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        }
        Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());

        /*Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 2);*/
    }

//    InputFilter filter = new InputFilter() {
//        public CharSequence filter(CharSequence source, int start, int end,
//                                   Spanned dest, int dstart, int dend) {
//            for (int i = start; i < end; i++) {
//                if (Character.isWhitespace(source.charAt(i))) {
//                    return "";
//                }
//            }
//            return null;
//        }
//
//    };


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(SignUpActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(SignUpActivity.this, getResources().getString(R.string.storage_permission_error));

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case galleryIntent://gallery
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        handleimage(uri);
                    } else {
                        Utils.toast(SignUpActivity.this, getResources().getString(R.string.unable_to_select_image));
                    }
                }
                break;
            case cameraIntent://camera
                /*if (data != null) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    profilePic.setColorFilter(getResources().getColor(R.color.transparent));
                    profilePic.setImageBitmap(imageBitmap);

                    Uri tempUri = Utils.getImageUri(getApplicationContext(), imageBitmap);


                    uploadFile = new File(Utils.getRealPathFromURI(SignUpActivity.this, tempUri));
                }*/

                uploadFile = new File(String.valueOf(appSettings.getImageUploadPath()));
                if (uploadFile.exists()) {
                    Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        profilePic.setColorFilter(ContextCompat.getColor(SignUpActivity.this, R.color.transparent));
                        Glide.with(SignUpActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profilePic);
                    } else {
                        profilePic.setColorFilter(getResources().getColor(R.color.transparent));
                        Glide.with(SignUpActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profilePic);
                    }
                }
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//                    if (uri != null) {
//                        beginCrop(uri);
//                    } else {
//                        Utils.toast(InitProfileActivity.this, getString(R.string.unable_to_select));
//                    }
//
//                } else {
//                    Log.d(TAG, "onActivityResult: " + appSettings.getImageValue());
//                    if (appSettings.getImageValue().trim().length() != 0) {
//                        File file = new File(appSettings.getImageValue());
//                        beginCrop(Uri.fromFile(file));
//                    } else {
//                        Utils.toast(InitProfileActivity.this, getString(R.string.unable_to_select));
//                    }
//
//                }

//                if (uriCamera != null) {
//                    uploadFile = new File(String.valueOf(uriCamera));
//                    Log.e("photo", "2: " + uriCamera);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                       /* Glide.with(SignUpActivity.this)
//                                .load(uriCamera)
//                                .placeholder(R.drawable.dp)
//                                .into(profilePic);*/
////                        profilePic.setColorFilter(ContextCompat.getColor(SignUpActivity.this, R.color.transparent));
////
////                        profilePic.setImageURI(null);
////                        profilePic.setImageURI(uriCamera);
//
//                        Glide.with(SignUpActivity.this).load(appSettings.getImageUploadPath()).placeholder(R.drawable.dp).into(profilePic);
//                    } else {
//                        profilePic.setColorFilter(getResources().getColor(R.color.transparent));
//                        Glide.with(SignUpActivity.this)
//                                .load(Utils.getRealPathFromUriNew(SignUpActivity.this, uriCamera))
//                                .into(profilePic);
//                    }
//
//                }
                break;
        }
    }

    private void handleimage(Uri uri) {
        profilePic.setColorFilter(getResources().getColor(R.color.transparent));
        Glide.with(SignUpActivity.this)
                .load(Utils.getRealPathFromUriNew(SignUpActivity.this, uri))
                .into(profilePic);
        uploadFile = new File(Utils.getRealPathFromURI(SignUpActivity.this, uri));

    }

//    public final static boolean isValidEmail(CharSequence target) {
//        if (target == null) {
//            return false;
//        } else {
//            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
//        }
//    }

    private void uploadImage() {
        ApiCall.uploadImage(uploadFile, SignUpActivity.this, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                uploadImagepath = response.optString("image");
                proceedSignUp();
            }
        });
    }


    private String validInputs() {
        String returnVal;
        if (firstNameEditText.getText().length() == 0) {
            returnVal = getResources().getString(R.string.please_enter_valid_firstname);
        } else if (lastNameEditText.getText().length() == 0) {
            returnVal = getResources().getString(R.string.please_enter_valid_lastname);
        } else if (emailEditText.getText().toString().trim().length() == 0 || !isValidEmail(emailEditText.getText().toString().trim())) {
            returnVal = getResources().getString(R.string.please_enter_valid_email);
        } else if (passwordEditText.getText().length() == 0) {
            returnVal = getResources().getString(R.string.please_enter_valid_password);
        } else if (passwordEditText.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);

        } else if (phoneEditText.getText().length() == 0) {
            returnVal = getResources().getString(R.string.please_enter_valid_phoneNumber);
        } else {
            returnVal = "true";
        }
        return returnVal;
    }

    private void proceedSignUp() {
        ApiCall.PostMethod(SignUpActivity.this, UrlHelper.SIGN_UP, getSignUpInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Utils.toast(SignUpActivity.this, getResources().getString(R.string.account_created_successfully));
                    }
                });
                moveToSignIn();
            }
        });

//        ApiCall.uploadImage(uploadFile, SignUpActivity.this, new VolleyCallback() {
//            @Override
//            public void onSuccess(JSONObject response) {
//                Utils.log(TAG, "response:" + response);
//            }
//        });
    }

    private JSONObject getSignUpInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("first_name", firstNameEditText.getText().toString());
            jsonObject.put("last_name", lastNameEditText.getText().toString());
            jsonObject.put("email", emailEditText.getText().toString().trim());
            jsonObject.put("password", passwordEditText.getText().toString());
            jsonObject.put("mobile", phoneEditText.getText().toString());
            jsonObject.put("image", uploadImagepath);
            Utils.log(TAG, "getSignupInputs:" + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void moveToSignIn() {
        Intent signin = new Intent(SignUpActivity.this, SignInActivity.class);
        startActivity(signin);
        finish();

    }
}
