package com.pyramidions.uberdooX.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.koushikdutta.ion.Ion;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.helpers.DataParser;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TrackingActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    GoogleMap map;
    TextView providerName, distanceView, etaView;
    String mobileNumber;
    ImageView callButton;
    AppSettings appSettings = new AppSettings(TrackingActivity.this);
    private android.location.LocationManager locationManager;
    private String TAG = TrackingActivity.class.getSimpleName();
    private String dist, dura, provider_id;
    private LatLng origin, dest;
    private Socket socket;
    String img;
    ImageView dummyIcon;
    private MarkerOptions srcmarkerOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = true;
        try {
            socket = IO.socket(UrlHelper.SOCKET_URL, opts);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.e("SOCKET.IO ", e.getMessage());
        }

//        socket= AppController.getSocket();
        socket.connect();


        distanceView = (TextView) findViewById(R.id.distance);
        etaView = (TextView) findViewById(R.id.eta);
        callButton = (ImageView) findViewById(R.id.callButton);
        dummyIcon = (ImageView) findViewById(R.id.dummyIcon);
        mobileNumber = getIntent().getStringExtra("mobileNumber");
        provider_id = getIntent().getStringExtra("provider_id");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 106);
            }
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
        ImageView backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        getDistanceInfo(getIntent().getStringExtra("src_lat"),getIntent().getStringExtra("src_lng"),getIntent().getStringExtra("des_lat"),getIntent().getStringExtra("des_lng"));

        getDistanceInfo(getIntent().getStringExtra("src_lat"), getIntent().getStringExtra("src_lng"), getIntent().getStringExtra("des_lat"), getIntent().getStringExtra("des_lng"));
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
                        "tel", mobileNumber, null));
                startActivity(phoneIntent);
            }
        });

        img = getIntent().getStringExtra("provider_image");
        double src_lat = Double.parseDouble(getIntent().getStringExtra("src_lat"));
        double src_lng = Double.parseDouble(getIntent().getStringExtra("src_lng"));


//        final Handler handler=new Handler();
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    getLocationFromServer();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                handler.postDelayed(this,30000);
//            }
//        });

        try {
            getLocationFromServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.on("GetLocation-" + provider_id, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject response = (JSONObject) args[0];
                Log.d(TAG, "call: " + response.optString("latitude") + "," + response.optString("longitude"));
                double src_lat = Double.parseDouble(response.optString("latitude"));
                double src_lng = Double.parseDouble(response.optString("longitude"));

                double des_lat = Double.parseDouble(getIntent().getStringExtra("des_lat"));
                double des_lng = Double.parseDouble(getIntent().getStringExtra("des_lng"));
                origin = new LatLng(src_lat, src_lng);
                dest = new LatLng(des_lat, des_lng);

                getDistanceInfo("" + src_lat, "" + src_lng, "" + des_lat, "" + des_lng);
                String url = getUrl(origin, dest);
                FetchUrl FetchUrl = new FetchUrl();

                // Start downloading json data from Google Directions API
                FetchUrl.execute(url);
            }
        });
//        double src_lat = Double.parseDouble("12.9673");
//        double src_lng = Double.parseDouble("80.1527");


        double des_lat = Double.parseDouble(getIntent().getStringExtra("des_lat"));
        double des_lng = Double.parseDouble(getIntent().getStringExtra("des_lng"));
        origin = new LatLng(src_lat, src_lng);
        dest = new LatLng(des_lat, des_lng);
        // Getting URL to the Google Directions API
        String url = getUrl(origin, dest);
        Log.d("onMapClick", url.toString());
//        FetchUrl FetchUrl = new FetchUrl();

        // Start downloading json data from Google Directions API
//        FetchUrl.execute(url);

    }


    public Bitmap getBitmapImage()

    {
        Bitmap bmImg = null;
        if (img.trim().length() == 0) {


            bmImg = BitmapFactory.decodeResource(TrackingActivity.this.getResources(),
                    R.drawable.dp);


            return getCircleBitmap(bmImg);

        } else {
            try {
                bmImg = Ion.with(TrackingActivity.this)
                        .load(img).asBitmap().get();


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return getCircleBitmap(bmImg);
        }
    }


    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, 60, 60);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    private void getLocationFromServer() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("provider_id", provider_id);
        ApiCall.PostMethodHeaders(TrackingActivity.this, UrlHelper.GET_PROVIDER_LOCATION, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                Log.d(TAG, "call: " + response.optString("latitude") + "," + response.optString("longitude"));
                double src_lat = Double.parseDouble(response.optString("latitude"));
                double src_lng = Double.parseDouble(response.optString("longitude"));

                double des_lat = Double.parseDouble(getIntent().getStringExtra("des_lat"));
                double des_lng = Double.parseDouble(getIntent().getStringExtra("des_lng"));
                origin = new LatLng(src_lat, src_lng);
                dest = new LatLng(des_lat, des_lng);

                getDistanceInfo("" + src_lat, "" + src_lng, "" + des_lat, "" + des_lng);
                String url = getUrl(origin, dest);
                FetchUrl FetchUrl = new FetchUrl();

                // Start downloading json data from Google Directions API
                FetchUrl.execute(url);
            }
        });
    }


    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "?key=" + getResources().getString(R.string.google_maps_key);


        return url;
    }


    private String getDistanceInfo(String src_lat, String src_lng, String des_lat, String des_lng) {
        try {

            String url = "http://maps.googleapis.com/maps/api/directions/json?origin=" + src_lat + "," + src_lng + "&destination=" + des_lat + "," + des_lng + "&mode=driving&sensor=false?key=" + getResources().getString(R.string.google_maps_key);
            Utils.log(TAG, "url: " + url);

            ApiCall.getMethod(TrackingActivity.this, url, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = response;
                    try {
                        Utils.log(TAG, "jsonObject: " + response);
                        JSONArray array = response.optJSONArray("routes");
                        JSONObject routes = array.getJSONObject(0);
                        JSONArray legs = routes.getJSONArray("legs");
                        JSONObject steps = legs.getJSONObject(0);
                        JSONObject distance = steps.getJSONObject("distance");
                        JSONObject duration = steps.getJSONObject("duration");

                        Log.i("Distance", distance.toString());
                        dist = distance.getString("text");
                        dura = duration.getString("text");
                        distanceView.setText(dist + " " + getResources().getString(R.string.away_from));
                        etaView.setText(dura + " " + getResources().getString(R.string.minutes_to_reach));

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {

        }
        return dist;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.map));

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onLocationChanged(Location location) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(false);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
            }
        }
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            map.clear();

            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLACK);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }
            int height = 100;
            int width = 80;
            final BitmapDrawable srcBitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.tracl_location_2);
            BitmapDrawable desBitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.track_marker);
            final Bitmap b = srcBitmap.getBitmap();
            Bitmap ab = desBitmap.getBitmap();
            Bitmap srcsmallMarker;
            final Bitmap dessmallMarker = Bitmap.createScaledBitmap(ab, width, height, false);
            Transformation transformation = new RoundedTransformationBuilder()
                    .borderColor(Color.WHITE)
                    .borderWidthDp(3)
                    .cornerRadiusDp(40)
                    .oval(false)
                    .build();
            if (img.trim().length()==0)
            {
                Picasso.with(TrackingActivity.this).load("http://104.131.74.144/uber_test/profile_pic.png").transform(transformation).resize(140, 140).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        srcmarkerOptions = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                                .anchor(0.5f, 0.5f);
                        srcmarkerOptions.position(origin);

                        map.addMarker(srcmarkerOptions);

                        MarkerOptions desmarkerOptions = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromBitmap(dessmallMarker))
                                .anchor(0.5f, 0.5f);


                        // Setting position on the MarkerOptions


                        desmarkerOptions.position(dest);
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();

                        builder.include(desmarkerOptions.getPosition());
                        builder.include(srcmarkerOptions.getPosition());
                        LatLngBounds bounds = builder.build();
                        int widths = getResources().getDisplayMetrics().widthPixels;
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100);


                        map.animateCamera(cu);
                        map.addMarker(desmarkerOptions);


                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            } else {
                Picasso.with(TrackingActivity.this).load(img).transform(transformation).resize(140, 140).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        srcmarkerOptions = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                                .anchor(0.5f, 0.5f);
                        srcmarkerOptions.position(origin);

                        map.addMarker(srcmarkerOptions);

                        MarkerOptions desmarkerOptions = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromBitmap(dessmallMarker))
                                .anchor(0.5f, 0.5f);


                        // Setting position on the MarkerOptions


                        desmarkerOptions.position(dest);
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();

                        builder.include(desmarkerOptions.getPosition());
                        builder.include(srcmarkerOptions.getPosition());
                        LatLngBounds bounds = builder.build();
                        int widths = getResources().getDisplayMetrics().widthPixels;
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100);


                        map.animateCamera(cu);
                        map.addMarker(desmarkerOptions);


                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }


            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                map.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }

    private LatLng midPoint(double lat1, double long1, double lat2, double long2) {

        return new LatLng((lat1 + lat2) / 2, (long1 + long2) / 2);

    }
}
