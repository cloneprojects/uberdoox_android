package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.MapActivity;
import com.pyramidions.uberdooX.activities.SelectTimeAndAddressActivity;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * Created by user on 23-10-2017.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {
    private Context context;
    private JSONArray addresses;
    private String TAG = AddressAdapter.class.getSimpleName();

    public AddressAdapter(Context context, JSONArray addresses) {
        this.context = context;
        this.addresses = addresses;
    }

    @Override
    public AddressAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_address, parent, false);

        return new AddressAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressAdapter.MyViewHolder holder, final int position) {
        final JSONObject address = addresses.optJSONObject(position);
        final AppSettings appSettings = new AppSettings(context);

        Utils.log(TAG, "address:" + address);
        holder.address.setText(address.optString("address_line_1"));
        holder.title.setText(address.optString("title"));
//        holder.location.setText(address.optString("address_line_1"));
//        holder.city.setText(address.optString("city"));
//        holder.pincode.setText(address.optString("pincode"));

        if (address.optString("selected").equalsIgnoreCase("true")) {
            Drawable drawable1 = context.getResources().getDrawable(R.drawable.bg_selected);
            drawable1.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(context), PorterDuff.Mode.SRC_IN));
            holder.rootLayout.setBackground(drawable1);
            //holder.rootLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.selectedImage.setVisibility(View.VISIBLE);
            SelectTimeAndAddressActivity.isAddressSelected = true;
        } else {
            Drawable drawable1 = context.getResources().getDrawable(R.drawable.bg_selected);
            drawable1.setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.grey2),
                    PorterDuff.Mode.SRC_IN));
            holder.rootLayout.setBackground(drawable1);
            // holder.rootLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.selectedImage.setVisibility(View.GONE);
        }
        if (address.optString("type").equalsIgnoreCase("addaddress")) {
            holder.addAddressView.setVisibility(View.VISIBLE);
            holder.address.setVisibility(View.GONE);
            holder.location.setVisibility(View.GONE);
            holder.city.setVisibility(View.GONE);
            holder.title.setVisibility(View.GONE);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MapActivity.class);
                    intent.putExtra("from", "select");
                    context.startActivity(intent);
                }
            });
        } else {
            holder.addAddressView.setVisibility(View.GONE);
            holder.address.setVisibility(View.VISIBLE);
            holder.location.setVisibility(View.GONE);
            holder.city.setVisibility(View.GONE);
            holder.title.setVisibility(View.VISIBLE);


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appSettings.setSelectedlat(address.optString("latitude"));
                    appSettings.setSelectedLong(address.optString("longitude"));
                    appSettings.setSelectedCity(address.optString("city"));
                    appSettings.setSelectedAddressId(address.optString("id"));
                    appSettings.setSelectedAddressTitle(address.optString("title"));
                    appSettings.setSelectedAddress(address.optString("address_line_1"));

                    if (address.optString("selected").equalsIgnoreCase("true")) {

                    } else {
                        for (int i = 0; i < addresses.length(); i++) {
                            if (i != position) {
                                addresses.optJSONObject(i).remove("selected");
                                try {
                                    addresses.optJSONObject(i).put("selected", "false");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                addresses.optJSONObject(i).remove("selected");
                                try {
                                    addresses.optJSONObject(i).put("selected", "true");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                    }
                    notifyDataSetChanged();
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return addresses.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView address, location, city, pincode, title, addresss_add_tv;
        RelativeLayout rootLayout;
        RelativeLayout rootLayout2;
        ImageView selectedImage, addresss_add;
        LinearLayout addAddressView;

        MyViewHolder(View view) {
            super(view);
            addAddressView = view.findViewById(R.id.addAddressView);
            selectedImage = view.findViewById(R.id.selectedImage);
            addresss_add = view.findViewById(R.id.addresss_add);
            addresss_add_tv = view.findViewById(R.id.address_add_tv);
            Utils.setIconColour(context, selectedImage.getDrawable());
            Utils.setIconColour(context, addresss_add.getDrawable());
            Utils.setTextColour(context, addresss_add_tv);
            address = view.findViewById(R.id.address);
            location = view.findViewById(R.id.location);
            city = view.findViewById(R.id.city);
//            pincode = view.findViewById(R.id.pincode);
            title = view.findViewById(R.id.title);
            rootLayout = view.findViewById(R.id.rootLayout);
            rootLayout2 = view.findViewById(R.id.rootLayout2);
        }
    }
}