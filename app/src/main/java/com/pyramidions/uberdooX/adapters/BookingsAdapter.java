package com.pyramidions.uberdooX.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.activities.DetailedBookingActivity;
import com.pyramidions.uberdooX.activities.TrackingActivity;
import com.pyramidions.uberdooX.fragments.BookingsFragment;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by karthik on 07/10/17.
 */
public class BookingsAdapter extends RecyclerView.Adapter<BookingsAdapter.MyViewHolder> {
    private Context context;
    private JSONArray subCategories;
    private String TAG = BookingsAdapter.class.getSimpleName();

    public BookingsAdapter(Context context, JSONArray subCategories) {
        this.context = context;
        this.subCategories = subCategories;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_booking, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try {
            Utils.log(TAG, "values: " + subCategories.optJSONObject(position));
            String providerName, eta, distance, phone_number, prvoider_pic;
            providerName = subCategories.optJSONObject(position).optString("providername");
            prvoider_pic = subCategories.optJSONObject(position).optString("image");
            eta = subCategories.optJSONObject(position).optString("eta");
            distance = subCategories.optJSONObject(position).optString("distance");
            phone_number = subCategories.optJSONObject(position).optString("phone_number");


            holder.serviceName.setText(subCategories.optJSONObject(position).optString("sub_category_name"));
            holder.serviceTiming.setText(subCategories.optJSONObject(position).optString("timing"));
            holder.providerName.setText(providerName);
            holder.serviceDate.setText(subCategories.optJSONObject(position).optString("booking_date"));

            Glide.with(context).load(prvoider_pic).dontAnimate().into(holder.providerPic);


        } catch (Exception e) {
            e.printStackTrace();
        }
//
//
        String status = subCategories.optJSONObject(position).optString("status");
        int colorvalue = 0;
        String statusValue = "";
        Drawable statusIcon = null;

//        String descValue = "";

//
        if (status.equalsIgnoreCase("Pending")) {
            statusValue = context.getResources().getString(R.string.pending);
            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_pending);


        } else if (status.equalsIgnoreCase("Rejected")) {

            statusValue = context.getResources().getString(R.string.rejected);
            colorvalue = context.getResources().getColor(R.color.red);
            statusIcon = context.getResources().getDrawable(R.drawable.new_cancelled);


        } else if (status.equalsIgnoreCase("Accepted")) {
            statusValue = context.getResources().getString(R.string.accepted);
            colorvalue = context.getResources().getColor(R.color.green);
            statusIcon = context.getResources().getDrawable(R.drawable.new_accepted);

        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
            statusValue = context.getResources().getString(R.string.cancelled) + " " + context.getResources().getString(R.string.by_user);
            colorvalue = context.getResources().getColor(R.color.red);
            statusIcon = context.getResources().getDrawable(R.drawable.new_cancelled);

        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
            statusValue = context.getResources().getString(R.string.cancelled) + " " + context.getResources().getString(R.string.by_provider);
            colorvalue = context.getResources().getColor(R.color.red);
            statusIcon = context.getResources().getDrawable(R.drawable.new_cancelled);


        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {
            statusValue = context.getResources().getString(R.string.on_the_way);
            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_start_to_place);

        } else if (status.equalsIgnoreCase("Startedjob")) {
            statusValue = context.getResources().getString(R.string.job_started);
            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_started_job);

        } else if (status.equalsIgnoreCase("Completedjob")) {
            statusValue = context.getResources().getString(R.string.completed);
            colorvalue = context.getResources().getColor(R.color.green);
            statusIcon = context.getResources().getDrawable(R.drawable.new_complete_job);
        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
            statusValue = context.getResources().getString(R.string.waiting_for);
            colorvalue = context.getResources().getColor(R.color.status_orange);
            statusIcon = context.getResources().getDrawable(R.drawable.new_pay_conifrmation);
        } else if (status.equalsIgnoreCase("Reviewpending")) {
            statusValue = context.getResources().getString(R.string.review_pending);
            colorvalue = context.getResources().getColor(R.color.ratingColor);
            statusIcon = context.getResources().getDrawable(R.drawable.new_review);
        } else if (status.equalsIgnoreCase("Finished")) {
            statusValue = context.getResources().getString(R.string.finished);
            colorvalue = context.getResources().getColor(R.color.green);
            statusIcon = context.getResources().getDrawable(R.drawable.new_finished);

        }

        holder.statusText.setText(statusValue);
        holder.statusText.setTextColor(colorvalue);
        holder.statusIcon.setImageDrawable(statusIcon);

//
//        holder.bookingDescription.setText(descValue);
//
//        if (status.equalsIgnoreCase("Rejected")) {
//            setTextVisibility(1, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rejected));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.red));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.rejected));
//
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(false);
//
//
//        } else if (status.equalsIgnoreCase("CancelledbyUser")) {
//            setTextVisibility(1, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rejected));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.red));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.cancelled));
//
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(false);
//
//
//        } else if (status.equalsIgnoreCase("CancelledbyProvider")) {
//            setTextVisibility(1, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rejected));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.red));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.cancelled));
//
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(false);
//
//
//        } else if (status.equalsIgnoreCase("Pending")) {
//            setTextVisibility(1, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pending_orange));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.pending));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//
//
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(true);
//
//
//        } else if (status.equalsIgnoreCase("Accepted")) {
//            setTextVisibility(1, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_blue));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_grey));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.blue));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(true);
//
//        } else if (status.equalsIgnoreCase("StarttoCustomerPlace")) {
//            setTextVisibility(2, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_orange));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_grey));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_grey));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.grey));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.on_the_way));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//
//
//            holder.track.setEnabled(true);
//            holder.cancel.setEnabled(false);
//
//
//        } else if (status.equalsIgnoreCase("Startedjob")) {
//            setTextVisibility(3, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_job_orange));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.complete));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.start_job));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//
//            holder.track.setEnabled(true);
//
//            holder.cancel.setEnabled(false);
//
//        } else if (status.equalsIgnoreCase("Completedjob")) {
//            setTextVisibility(4, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_blue));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_orange));
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.blue));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.grey));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.review));
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(false);
//
//
//        } else if (status.equalsIgnoreCase("Waitingforpaymentconfirmation")) {
//            setTextVisibility(4, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_black));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_orange));
//
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.orange));
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(false);
//
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.pay));
//        } else if (status.equalsIgnoreCase("Reviewpending")) {
//            setTextVisibility(4, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_black));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.pay_orange));
//
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.orange));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.review));
//        } else {
//            setTextVisibility(4, holder.isInPending, holder.isOnTheWay, holder.isInProgress, holder.isCompleted);
//
//            holder.isConfirmedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.accepted_black));
//            holder.isOnTheWayImage.setImageDrawable(context.getResources().getDrawable(R.drawable.start_to_place_black));
//            holder.isInProgressImage.setImageDrawable(context.getResources().getDrawable(R.drawable.job_black));
//            holder.isCompletedImage.setImageDrawable(context.getResources().getDrawable(R.drawable.payment_blue));
//
//
//            holder.isInPending.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isOnTheWay.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isInProgress.setTextColor(context.getResources().getColor(R.color.black));
//            holder.isCompleted.setTextColor(context.getResources().getColor(R.color.blue));
//
//            holder.isInPending.setText(context.getResources().getString(R.string.accepted));
//            holder.isOnTheWay.setText(context.getResources().getString(R.string.start_to_place));
//            holder.isInProgress.setText(context.getResources().getString(R.string.completed));
//            holder.isCompleted.setText(context.getResources().getString(R.string.finished));
//            holder.track.setEnabled(false);
//            holder.cancel.setEnabled(false);
//
//
//        }
//        Glide.with(context).load(subCategories.optJSONObject(position).optString("icon")).into(holder.serviceImage);
//
//
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailedBooking = new Intent(context, DetailedBookingActivity.class);
                detailedBooking.putExtra("bookingValues", subCategories.optJSONObject(position).toString());
                context.startActivity(detailedBooking);
            }
        });
//
//        holder.track.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent tracking = new Intent(context, TrackingActivity.class);
//                tracking.putExtra("des_lat",subCategories.optJSONObject(position).optString("boooking_latitude"));
//                tracking.putExtra("des_lng",subCategories.optJSONObject(position).optString("booking_longitude"));
//                tracking.putExtra("src_lat",subCategories.optJSONObject(position).optString("provider_latitude"));
//                tracking.putExtra("src_lng",subCategories.optJSONObject(position).optString("provider_longitude"));
//                tracking.putExtra("providerName",subCategories.optJSONObject(position).optString("providername"));
//                tracking.putExtra("mobileNumber",subCategories.optJSONObject(position).optString("provider_mobile"));
//                tracking.putExtra("provider_id",subCategories.optJSONObject(position).optString("provider_id"));
//                context.startActivity(tracking);
//            }
//        });
//
//        holder.cancel.setOnClickListener(new View.OnClickListener() {
//                                             @Override
//                                             public void onClick(View view) {
//
//                                                 showCancelDialog(subCategories.optJSONObject(position).optString("id"));
//                                             }
//                                         }
//        );


    }


    private void showCancelDialog(final String id) {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.cancel_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView yesButton, noButton;
        yesButton = (TextView) dialog.findViewById(R.id.yesButton);
        noButton = (TextView) dialog.findViewById(R.id.noButton);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                canceBookings(id);
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }


    private void canceBookings(String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(context, UrlHelper.CANCEL_BOOKING, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                BookingsFragment.getData(context);
            }
        });
    }

    public void setTextVisibility(int num, TextView first, TextView second, TextView third, TextView four) {
        if (num == 1) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.GONE);
            third.setVisibility(View.GONE);
            four.setVisibility(View.GONE);
        } else if (num == 2) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.VISIBLE);
            third.setVisibility(View.GONE);
            four.setVisibility(View.GONE);
        } else if (num == 3) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.VISIBLE);
            third.setVisibility(View.VISIBLE);
            four.setVisibility(View.GONE);
        } else if (num == 4) {
            first.setVisibility(View.VISIBLE);
            second.setVisibility(View.VISIBLE);
            third.setVisibility(View.VISIBLE);
            four.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return subCategories.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView serviceName, serviceTiming, statusText, serviceDate;
        ImageView serviceImage, statusIcon;
        RoundedImageView providerPic;
        ImageView isCompletedImage, isInProgressImage, isOnTheWayImage, isConfirmedImage;
        TextView isInPending, isOnTheWay, isInProgress, isCompleted, bookingDescription, providerName;
        Button track, cancel;


        MyViewHolder(View view) {
            super(view);
            statusText = view.findViewById(R.id.statusText);
            providerPic = view.findViewById(R.id.providerPic);
            // Utils.setTextColour(context, statusText);
            Utils.setIconColour(context, providerPic.getDrawable());
            providerName = view.findViewById(R.id.providerName);
            serviceName = view.findViewById(R.id.serviceName);
            serviceTiming = view.findViewById(R.id.serviceTiming);
            serviceDate = view.findViewById(R.id.serviceDate);
            statusIcon = view.findViewById(R.id.statusIcon);
//            serviceImage = view.findViewById(R.id.serviceIcon);
//            isCompletedImage = view.findViewById(R.id.isCompletedImage);
//            isInProgressImage = view.findViewById(R.id.isInProgressImage);
//            isOnTheWayImage = view.findViewById(R.id.isOnTheWayImage);
//            isConfirmedImage = view.findViewById(R.id.isConfirmedImage);
//            isInPending = view.findViewById(R.id.isInPending);
//            isOnTheWay = view.findViewById(R.id.isOnTheWay);
//            isInProgress = view.findViewById(R.id.isInProgress);
//            isCompleted = view.findViewById(R.id.isCompleted);
//            track = view.findViewById(R.id.track);
//            cancel = view.findViewById(R.id.cancel);
//            bookingDescription = view.findViewById(R.id.bookingDescription);
        }
    }
}
