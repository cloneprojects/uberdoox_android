package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.Events.Favorite;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.SubCategoriesActivity;
import com.pyramidions.uberdooX.helpers.DatabaseHandler;
import com.pyramidions.uberdooX.helpers.Utils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okio.Utf8;

/**
 * Created by karthik on 06/10/17.
 */
public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {
    private Context context;
    private JSONArray categories;
    private boolean likedStatus = false;
    private DatabaseHandler db;

    public CategoriesAdapter(Context context, JSONArray categories) {
        this.context = context;
        this.categories = categories;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        ImageView categoryImage, favorite_icon;
        LinearLayout categoryLayout;
        RelativeLayout favorite_layout;

        MyViewHolder(View view) {
            super(view);
            categoryName = view.findViewById(R.id.categoryName);
            favorite_icon = view.findViewById(R.id.favorite_icon);
            categoryImage = view.findViewById(R.id.categoryImage);
            favorite_layout = view.findViewById(R.id.favorite_layout);
            categoryLayout = view.findViewById(R.id.categoryLayout);
            Utils.setRLColour(context, favorite_layout);
        }
    }

    @Override
    public CategoriesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);
        db = new DatabaseHandler(context);
        return new CategoriesAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final CategoriesAdapter.MyViewHolder holder, int position) {
        try {
            final JSONObject subCategory = categories.getJSONObject(position);
            Log.d("onBindViewHolder: ", "jsonObject" + subCategory);
            holder.categoryName.setText(subCategory.optString("category_name"));

            Glide.with(context).load(subCategory.optString("icon")).into(holder.categoryImage);
//            Glide.with(context).load("http://bmseelectrical.com/assets/uploads/image_uploads/ElectricalInstallation.jpg").into(holder.categoryImage);
            holder.categoryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent timeAndAddress = new Intent(context, SubCategoriesActivity.class);
                    timeAndAddress.putExtra("subcategoriesArray", categories.toString());
                    timeAndAddress.putExtra("subCategoryId", subCategory.optString("id"));
                    context.startActivity(timeAndAddress);

                }
            });


            if (db.isLiked(subCategory.optString("id"), "true")) {
                holder.favorite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.filled));

            } else {
                holder.favorite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.unfilled));

            }

            holder.favorite_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!db.isLiked(subCategory.optString("id"), "true")) {

                        db.insertData(subCategory.optString("id"), subCategory.optString("category_name"), subCategory.optString("icon"));
                        holder.favorite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.filled));
                        Toast.makeText(context, R.string.added_favorites, Toast.LENGTH_SHORT).show();

                    } else {
                        db.deleteData(subCategory.optString("id"), "true");
                        holder.favorite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.unfilled));

                    }
                    EventBus.getDefault().post(new Favorite());
                    notifyDataSetChanged();

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return categories.length();
    }
}


