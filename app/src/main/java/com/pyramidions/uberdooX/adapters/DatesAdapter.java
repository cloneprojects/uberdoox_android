package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pyramidions.uberdooX.Events.onSelected;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.SelectTimeAndAddressActivity;
import com.pyramidions.uberdooX.helpers.CircularTextView;
import com.pyramidions.uberdooX.helpers.Utils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 23-10-2017.
 */

public class DatesAdapter extends RecyclerView.Adapter<DatesAdapter.MyViewHolder> {
    private Context context;
    private JSONArray dates;
    private String TAG = DatesAdapter.class.getSimpleName();

    public DatesAdapter(Context context, JSONArray dates) {
        this.context = context;
        this.dates = dates;
    }

    @Override
    public DatesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_date_slots, parent, false);

        return new DatesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DatesAdapter.MyViewHolder holder, final int position) {
        try {
            final AppSettings appSettings = new AppSettings(context);

            final JSONObject singledate = dates.getJSONObject(position);
            Log.d(TAG, "onBindViewHolder: " + singledate);
            holder.date.setText(singledate.optString("date"));
            holder.day.setText(singledate.optString("day"));
            if (singledate.optString("selected").equalsIgnoreCase("true")) {
                if (position == 0) {
                    SelectTimeAndAddressActivity.isTodaySelected = true;
                } else {
                    SelectTimeAndAddressActivity.isTodaySelected = false;

                }
                SelectTimeAndAddressActivity.isDateSeleceted = true;
                appSettings.setSelectedDate(singledate.optString("fullDate"));

                // holder.date.setStrokeWidth(0);
                holder.date.setTextColor(Color.parseColor("#ffffff"));
                // holder.date.setSolidColor("#6B7FFC");
                Drawable drawable1 = context.getResources().getDrawable(R.drawable.text_circular);
                drawable1.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(context), PorterDuff.Mode.SRC_IN));
                holder.date.setBackground(drawable1);

            } else {

                //  holder.date.setStrokeWidth(0);
                //   holder.date.setSolidColor("#006B7FFC");
                Drawable drawable1 = context.getResources().getDrawable(R.drawable.text_circular);
                drawable1.setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.white),
                        PorterDuff.Mode.SRC_IN));
                holder.date.setBackground(drawable1);
                holder.date.setTextColor(context.getResources().getColor(R.color.grey));

            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectTimeAndAddressActivity.isTimeSelected = false;

                    if (singledate.optString("selected").equalsIgnoreCase("true")) {
                        if (position == 0) {
                            SelectTimeAndAddressActivity.isTodaySelected = true;
                        } else {
                            SelectTimeAndAddressActivity.isTodaySelected = false;

                        }
                    } else {
                        if (position == 0) {
                            SelectTimeAndAddressActivity.isTodaySelected = true;
                        } else {
                            SelectTimeAndAddressActivity.isTodaySelected = false;

                        }
                        for (int i = 0; i < dates.length(); i++) {
                            if (i != position) {
                                dates.optJSONObject(i).remove("selected");
                                try {
                                    dates.optJSONObject(i).put("selected", "false");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                dates.optJSONObject(i).remove("selected");
                                SelectTimeAndAddressActivity.isDateSeleceted = true;
                                try {
                                    dates.optJSONObject(i).put("selected", "true");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                        EventBus.getDefault().post(new onSelected("no"));

                    }
                    notifyDataSetChanged();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dates.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView day;
        TextView date;

        MyViewHolder(View view) {
            super(view);
            date = view.findViewById(R.id.date);
            day = view.findViewById(R.id.day);
        }
    }
}