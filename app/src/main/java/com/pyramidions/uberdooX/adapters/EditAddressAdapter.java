package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AddressActivity;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.MapActivity;
import com.pyramidions.uberdooX.activities.SelectTimeAndAddressActivity;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 23-10-2017.
 */

public class EditAddressAdapter extends RecyclerView.Adapter<EditAddressAdapter.MyViewHolder> {
    private Context context;
    private JSONArray addresses;
    private String TAG = EditAddressAdapter.class.getSimpleName();

    public EditAddressAdapter(Context context, JSONArray addresses) {
        this.context = context;
        this.addresses = addresses;
    }

    @Override
    public EditAddressAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_items, parent, false);

        return new EditAddressAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EditAddressAdapter.MyViewHolder holder, final int position) {
        final JSONObject address = addresses.optJSONObject(position);
        Utils.log(TAG, "address:" + address);
        holder.address.setText(address.optString("address_line_1"));
        holder.addressTitle.setText(address.optString("title"));
        holder.editLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MapActivity.class);
                intent.putExtra("from", "edit");
                intent.putExtra("id", address.optString("id"));
                intent.putExtra("title", address.optString("title"));
                intent.putExtra("doorNo", address.optString("doorno"));
                intent.putExtra("landMark", address.optString("landmark"));
                intent.putExtra("latitude", address.optString("latitude"));
                intent.putExtra("longitude", address.optString("longitude"));
                context.startActivity(intent);
            }
        });

        holder.deleteLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AddressActivity.deleteId(context, address.optString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return addresses.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView address, addressTitle;
        LinearLayout deleteLay, editLay;

        MyViewHolder(View view) {
            super(view);
            address = view.findViewById(R.id.address);
            deleteLay = view.findViewById(R.id.deleteLay);
            editLay = view.findViewById(R.id.editLay);
            addressTitle = view.findViewById(R.id.addressTitle);
        }
    }
}