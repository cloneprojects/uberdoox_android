package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.SubCategoriesActivity;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 06/10/17.
 */
public class MainCategoriesAdapter extends RecyclerView.Adapter<MainCategoriesAdapter.MyViewHolder> {
    private Context context;
    private JSONArray categories;

    public MainCategoriesAdapter(Context context, JSONArray categories) {
        this.context = context;
        this.categories = categories;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        RecyclerView subCategories;

        MyViewHolder(View view) {
            super(view);
            categoryName = view.findViewById(R.id.categoryName);
            Utils.setTextColour(context, categoryName);
            subCategories = view.findViewById(R.id.subCategories);
        }
    }

    @Override
    public MainCategoriesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_category_item, parent, false);

        return new MainCategoriesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainCategoriesAdapter.MyViewHolder holder, int position) {
        try {
            JSONObject category = categories.getJSONObject(position);

            holder.categoryName.setText(category.optString("name"));
            JSONArray subCategories = new JSONArray();
            subCategories = category.optJSONArray("subCategories");
            CategoriesAdapter categoriesAdapter = new CategoriesAdapter(context, subCategories);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.subCategories.setLayoutManager(layoutManager);
            holder.subCategories.setAdapter(categoriesAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return categories.length();
    }
}


