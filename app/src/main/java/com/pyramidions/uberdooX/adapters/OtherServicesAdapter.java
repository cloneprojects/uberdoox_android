package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yuvaraj on 18/12/17.
 */

public class OtherServicesAdapter extends RecyclerView.Adapter<OtherServicesAdapter.MyViewHolder> {
    private Context context;
    private JSONArray reviews;

    public OtherServicesAdapter(Context context, JSONArray reviews) {
        this.context = context;
        this.reviews = reviews;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView serviceName, priceValue;
        RatingBar ratingBar;
        ImageView dot;


        MyViewHolder(View view) {
            super(view);
            serviceName = view.findViewById(R.id.serviceName);
            priceValue = view.findViewById(R.id.priceValue);
            dot = view.findViewById(R.id.dot);
            Utils.setIconColour(context, dot.getDrawable());

        }
    }

    @Override
    public OtherServicesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_other_services, parent, false);

        return new OtherServicesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OtherServicesAdapter.MyViewHolder holder, int position) {
        try {
            JSONObject review = reviews.getJSONObject(position);
            holder.serviceName.setText(review.optString("sub_category_name"));
            holder.priceValue.setText("$" + review.optString("priceperhour") + " " + context.getResources().getString(R.string.price_per_hour));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return reviews.length();
    }
}
