package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 23-10-2017.
 */

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MyViewHolder> {
    private Context context;
    private JSONArray addresses;
    private String TAG = PaymentAdapter.class.getSimpleName();

    public PaymentAdapter(Context context, JSONArray addresses) {
        this.context = context;
        this.addresses = addresses;
    }

    @Override
    public PaymentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_items, parent, false);

        return new PaymentAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final PaymentAdapter.MyViewHolder holder, final int position) {

        Log.e(TAG, "onBindViewHolder:" + addresses);
        final JSONObject jsonObject = addresses.optJSONObject(position);
        final AppSettings appSettings = new AppSettings(context);

        holder.paymentName.setText(jsonObject.optString("payment_name"));

        if (jsonObject.optString("selected").equalsIgnoreCase("true")) {
            appSettings.setPaymentType(jsonObject.optString("payment_name"));
            holder.selected_border.setVisibility(View.VISIBLE);
            holder.selected_round.setVisibility(View.VISIBLE);

            if (jsonObject.optString("payment_name").equalsIgnoreCase("card")) {
                MainActivity.cardLayout.setVisibility(View.VISIBLE);
                MainActivity.isCardSelected = true;
            } else {
                MainActivity.cardLayout.setVisibility(View.INVISIBLE);
                MainActivity.isCardSelected = false;
            }

        } else {
            holder.selected_border.setVisibility(View.GONE);
            holder.selected_round.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "position:" + position);

                for (int i = 0; i < addresses.length(); i++) {
                    addresses.optJSONObject(i).remove("selected");
                    try {
                        addresses.optJSONObject(i).put("selected", "false");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                try {
                    addresses.optJSONObject(position).remove("selected");
                    addresses.optJSONObject(position).put("selected", "true");
                    notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "onClick: " + addresses);
            }
        });

    }

    @Override
    public int getItemCount() {
        return addresses.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView paymentName;
        ImageView selected_border, paymentIcon;
        FrameLayout selected_round;


        MyViewHolder(View view) {
            super(view);
            paymentName = (TextView) view.findViewById(R.id.paymentName);
            selected_border = (ImageView) view.findViewById(R.id.selected_border);
            paymentIcon = (ImageView) view.findViewById(R.id.paymentIcon);
            selected_round = (FrameLayout) view.findViewById(R.id.selected_round);
        }
    }
}