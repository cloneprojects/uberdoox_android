package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.ProviderProfileActivity;
import com.pyramidions.uberdooX.activities.ShowProvidersActivity;
import com.pyramidions.uberdooX.helpers.OnSwipeListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 11/10/17.
 */
public class ProvidersAdapter extends RecyclerView.Adapter<ProvidersAdapter.MyViewHolder> {
    private Context context;
    private JSONArray providers;

    private GestureDetectorCompat detector;
    private OnSwipeListener onSwipeListener = new OnSwipeListener() {

        @Override
        public boolean onSwipe(Direction direction) {

            Log.d("Swipe", direction.toString());
            // Possible implementation
            if (direction == Direction.left || direction == Direction.right) {
                // Do something COOL like animation or whatever you want
                // Refer to your view if needed using a global reference
                return true;
            } else if (direction == Direction.up || direction == Direction.down) {
                // Do something COOL like animation or whatever you want
                // Refer to your view if needed using a global reference
                if (direction == Direction.up) {
                    Log.d("SWIPE", "UP");
//                    ActivityCompat.finishAfterTransition(ProviderProfileActivity.this);
                }
                return true;
            }

            return super.onSwipe(direction);
        }
    };


    public ProvidersAdapter(Context context, JSONArray providers) {
        this.context = context;
        this.providers = providers;
        detector = new GestureDetectorCompat(context, onSwipeListener);
    }

    @Override
    public ProvidersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_provider, parent, false);

        return new ProvidersAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProvidersAdapter.MyViewHolder holder, int position) {
        try {
            final JSONObject subCategory = providers.getJSONObject(position);
            Log.e("ProvidersAdapter", "providers: " + subCategory);
            holder.providerName.setText(subCategory.optString("name"));

            double val = Double.parseDouble(providers.optJSONObject(position).optString("distance"));
            String disntac = String.format("%.2f", val);
            holder.distance.setText(disntac + " km");

            try {
                holder.ratingView.setRating(Float.parseFloat(subCategory.optString("avg_rating")));
            } catch (Exception e) {

            }

            Glide.with(context).load(providers.optJSONObject(position).optString("image"))
                    .placeholder(context.getResources().getDrawable(R.drawable.dp)).into(holder.providerPic);
//            Glide.with(context).load("https://images.pexels.com/photos/542282/pexels-photo-542282.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb").placeholder(context.getResources().getDrawable(R.drawable.profile_pic)).into(holder.providerPic);
            //            Glide.with(context).load("https://images.pexels.com/photos/542282/pexels-photo-542282.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb").placeholder(context.getResources().getDrawable(R.drawable.profile_pic)).into(holder.providerPic);
            holder.providerPic.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    Log.d("TOUCH", "UP");
                    return detector.onTouchEvent(motionEvent);
                }
            });
            holder.rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(context, ProviderProfileActivity.class);
                        intent.putExtra("providerDetails", subCategory.toString());
                        Pair<View, String> p1 = Pair.create((View) holder.providerName, "providerName");
                        Pair<View, String> p2 = Pair.create((View) holder.providerPic, "providerPic");
                        Pair<View, String> p3 = Pair.create((View) holder.rootLayout, "parentLayout");
                        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((ShowProvidersActivity) context, holder.providerPic, "providerPic");
                        context.startActivity(intent, optionsCompat.toBundle());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return providers.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView providerName, distance;
        RatingBar ratingView;
        ImageView providerPic;
        LinearLayout rootLayout;
        LinearLayout cardView;

        MyViewHolder(View view) {
            super(view);
            providerName = view.findViewById(R.id.providerName);
            distance = view.findViewById(R.id.distance);
            providerPic = view.findViewById(R.id.providerPic);
            rootLayout = view.findViewById(R.id.rootLayout);
            cardView = view.findViewById(R.id.cardLayout);
            ratingView = view.findViewById(R.id.ratingView);
        }
    }
}
