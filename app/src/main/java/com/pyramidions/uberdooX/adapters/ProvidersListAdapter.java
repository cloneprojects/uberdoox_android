package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.ProviderProfileActivity;
import com.pyramidions.uberdooX.activities.ShowProvidersActivity;
import com.pyramidions.uberdooX.helpers.OnSwipeListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 11/10/17.
 */
public class ProvidersListAdapter extends RecyclerView.Adapter<ProvidersListAdapter.MyViewHolder> {
    private Context context;
    private JSONArray providers;



    public ProvidersListAdapter(Context context, JSONArray providers) {
        this.context = context;
        this.providers = providers;
    }

    @Override
    public ProvidersListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.provider_list_view, parent, false);

        return new ProvidersListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProvidersListAdapter.MyViewHolder holder, int position) {
        try {
            final JSONObject subCategory = providers.getJSONObject(position);
            holder.providerName.setText(subCategory.optString("name"));
            holder.serviceType.setText(subCategory.optString("mobile"));

            double val= Double.parseDouble(providers.optJSONObject(position).optString("distance"));
            String disntac = String.format("%.2f", val);
            holder.distance.setText(disntac);

            try {
                holder.ratingView.setRating(Float.parseFloat(subCategory.optString("avg_rating")));
            }catch (Exception e)
            {

            }
            Glide.with(context).load(providers.optJSONObject(position).optString("image")).placeholder(context.getResources().getDrawable(R.drawable.dp)).into(holder.providerPic);
//            Glide.with(context).load("https://images.pexels.com/photos/542282/pexels-photo-542282.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb").placeholder(context.getResources().getDrawable(R.drawable.profile_pic)).into(holder.providerPic);



            holder.rootLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(context, ProviderProfileActivity.class);
                        intent.putExtra("providerDetails",subCategory.toString());
                        Pair<View, String> p1 = Pair.create((View) holder.providerName, "providerName");
                        Pair<View, String> p2 = Pair.create((View) holder.providerPic, "providerPic");
                        Pair<View, String> p3 = Pair.create((View) holder.rootLayout, "parentLayout");
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation((ShowProvidersActivity) context, p1, p2,p3);
                        ActivityCompat.startActivity(context, intent, options.toBundle());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return providers.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView providerName, distance,serviceType;
        RatingBar ratingView;
        ImageView providerPic;
        LinearLayout rootLayout;

        MyViewHolder(View view) {
            super(view);
            providerName = view.findViewById(R.id.providerName);
            serviceType = view.findViewById(R.id.serviceType);
            distance = view.findViewById(R.id.distance);
            providerPic = view.findViewById(R.id.providerPic);
            rootLayout = view.findViewById(R.id.rootLayout);
            ratingView = view.findViewById(R.id.ratingView);
        }
    }
}
