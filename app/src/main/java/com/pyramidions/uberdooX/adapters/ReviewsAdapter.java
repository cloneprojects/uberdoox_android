package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.fragments.ReviewsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yuvaraj on 18/12/17.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {
    private Context context;
    private JSONArray reviews;

    public ReviewsAdapter(Context context, JSONArray reviews) {
        this.context = context;
        this.reviews = reviews;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, reviewContent;
        RatingBar ratingBar;


        MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.reviewName);
            reviewContent = view.findViewById(R.id.reviewContent);
            ratingBar = view.findViewById(R.id.ratingBar);
        }
    }

    @Override
    public ReviewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reviews, parent, false);

        return new ReviewsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReviewsAdapter.MyViewHolder holder, int position) {
        try {
            JSONObject review = reviews.getJSONObject(position);
            holder.reviewContent.setText(review.optString("feedback"));
            holder.ratingBar.setRating(Float.parseFloat(review.optString("rating")));
            holder.name.setText(review.optString("username"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return reviews.length();
    }
}
