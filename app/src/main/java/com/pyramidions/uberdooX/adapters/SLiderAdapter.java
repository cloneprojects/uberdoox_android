package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.SelectTimeAndAddressActivity;
import com.pyramidions.uberdooX.activities.SubCategoriesActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by karthik on 06/10/17.
 */

public class SLiderAdapter extends RecyclerView.Adapter<SLiderAdapter.MyViewHolder> {
    private Context context;
    private JSONArray categories;
    private boolean faveList;
    private AppSettings appSettings;

    public SLiderAdapter(Context context, JSONArray categories, boolean fave) {
        this.context = context;
        this.categories = categories;
        faveList = fave;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        ImageView categoryImage;

        MyViewHolder(View view) {
            super(view);
            categoryName = view.findViewById(R.id.categoryName);
            categoryImage = view.findViewById(R.id.categoryImage);
        }
    }

    @Override
    public SLiderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.home_slider, parent, false);
        return new SLiderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SLiderAdapter.MyViewHolder holder, int position) {
        appSettings = new AppSettings(context);
        //   Log.e("tag", "onBindViewHolder: " + categories);
        final JSONObject jsonObject = categories.optJSONObject(position);

        Glide.with(context).load(jsonObject.optString("banner_logo")).into(holder.categoryImage);
        holder.categoryName.setText(jsonObject.optString("banner_name"));

        if (faveList) {
            final String subCate = jsonObject.optString("toSubCategory");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (subCate != null) {
                        if (subCate.equalsIgnoreCase("true")) {
                            Intent intent = new Intent(context, SubCategoriesActivity.class);
                            intent.putExtra("subcategoriesArray", categories.toString());
                            intent.putExtra("subCategoryId", jsonObject.optString("id"));
                            context.startActivity(intent);
                        } else {
                            Intent intent = new Intent(context, SelectTimeAndAddressActivity.class);
                            appSettings.setSelectedSubCategory(jsonObject.optString("subId"));
                            context.startActivity(intent);
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categories.length();
    }
}