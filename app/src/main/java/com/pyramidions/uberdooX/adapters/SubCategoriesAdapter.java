package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooX.Events.Favorite;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.SelectTimeAndAddressActivity;
import com.pyramidions.uberdooX.helpers.DatabaseHandler;
import com.pyramidions.uberdooX.helpers.Utils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 07/10/17.
 */
public class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesAdapter.MyViewHolder> {
    private Context context;
    private JSONArray subCategories;
    private boolean likedStatus = false;
    private DatabaseHandler db;

    public SubCategoriesAdapter(Context context, JSONArray subCategories) {
        this.context = context;
        this.subCategories = subCategories;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView subCategoryName;
        ImageView subCategoryImage, favourite_icon;
        LinearLayout subCategoryLayout;
        RelativeLayout favourite_layout;

        MyViewHolder(View view) {
            super(view);
            subCategoryName = view.findViewById(R.id.subCategoryName);
            subCategoryImage = view.findViewById(R.id.subCategoryImage);
            subCategoryLayout = view.findViewById(R.id.subCategoryLayout);
            favourite_layout = view.findViewById(R.id.favourite_layout);
            Utils.setRLColour(context, favourite_layout);
            favourite_icon = view.findViewById(R.id.favourite_icon);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_sub_category, parent, false);
        db = new DatabaseHandler(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            final AppSettings appSettings = new AppSettings(context);

            final JSONObject subCategory = subCategories.getJSONObject(position);
            Log.d("onBindViewHolder: ", "jsonObject" + subCategory);
            holder.subCategoryName.setText(subCategory.optString("sub_category_name"));

            Glide.with(context).load(subCategory.optString("icon")).into(holder.subCategoryImage);
            holder.subCategoryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appSettings.setSelectedSubCategory(subCategory.optString("id"));
                    appSettings.setSelectedSubCategoryName(subCategory.optString("sub_category_name"));
                    Intent timeAndAddress = new Intent(context, SelectTimeAndAddressActivity.class);
                    context.startActivity(timeAndAddress);

                }
            });

            if (db.isLiked(subCategory.optString("id"), "false")) {
                holder.favourite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.filled));

            } else {
                holder.favourite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.unfilled));

            }

            holder.favourite_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!db.isLiked(subCategory.optString("id"), "false")) {
                        db.insertSubData(subCategory.optString("id"), subCategory.optString("sub_category_name"), subCategory.optString("icon"));

                        holder.favourite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.filled));
                        Toast.makeText(context, context.getString(R.string.added_favorites), Toast.LENGTH_SHORT).show();

                    } else {
                        db.deleteData(subCategory.optString("id"), "false");
                        holder.favourite_icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.unfilled));
                    }

                    EventBus.getDefault().post(new Favorite());
                    notifyDataSetChanged();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return subCategories.length();
    }
}
