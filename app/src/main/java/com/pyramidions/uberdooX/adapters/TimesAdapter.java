package com.pyramidions.uberdooX.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pyramidions.uberdooX.Events.onSelected;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.SelectTimeAndAddressActivity;
import com.pyramidions.uberdooX.helpers.Utils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by user on 23-10-2017.
 */

public class TimesAdapter extends RecyclerView.Adapter<TimesAdapter.MyViewHolder> {
    private Context context;
    private JSONArray timeSlots;
    private String TAG = TimesAdapter.class.getSimpleName();

    public TimesAdapter(Context context, JSONArray timeSlots) {
        this.context = context;
        this.timeSlots = timeSlots;
    }

    @Override
    public TimesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_time_slots, parent, false);

        return new TimesAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final TimesAdapter.MyViewHolder holder, final int position) {
        try {
            final AppSettings appSettings = new AppSettings(context);

            final JSONObject singleSlot = timeSlots.getJSONObject(position);

            Log.d(TAG, "onBindViewHolder: " + singleSlot);
            holder.timeSlot.setText(singleSlot.optString("timing"));
            if (singleSlot.optString("selected").equalsIgnoreCase("true")) {
                //   holder.timeSlot.setBackgroundResource(R.drawable.blue_bg);
                Utils.setButtonColor(context, holder.timeSlot);
                holder.timeSlot.setTextColor(Color.parseColor("#ffffff"));
                SelectTimeAndAddressActivity.isTimeSelected = true;
                appSettings.setSelectedTimeSlot(singleSlot.optString("id"));
                appSettings.setSelectedTimeText(holder.timeSlot.getText().toString());

            } else {

                holder.timeSlot.setBackgroundResource(R.drawable.gray_bg);
                holder.timeSlot.setTextColor(Color.parseColor("#1d1d1d"));
            }

            holder.timeSlot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (singleSlot.optString("selected").equalsIgnoreCase("true")) {


                    } else {
                        for (int i = 0; i < timeSlots.length(); i++) {
                            if (i != position) {
                                timeSlots.optJSONObject(i).remove("selected");
                                try {
                                    timeSlots.optJSONObject(i).put("selected", "false");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                timeSlots.optJSONObject(i).remove("selected");
                                try {
                                    timeSlots.optJSONObject(i).put("selected", "true");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                    }
                    notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return timeSlots.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        Button timeSlot;

        MyViewHolder(View view) {
            super(view);
            timeSlot = view.findViewById(R.id.timeSlot);
        }
    }
}