package com.pyramidions.uberdooX.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.AboutUs;
import com.pyramidions.uberdooX.activities.AddressActivity;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.ChangePasswordActivity;
import com.pyramidions.uberdooX.activities.EditProfileActivity;
import com.pyramidions.uberdooX.activities.FaqActivity;
import com.pyramidions.uberdooX.activities.MainActivity;
import com.pyramidions.uberdooX.activities.SignInActivity;
import com.pyramidions.uberdooX.activities.TermsAndConditions;
import com.pyramidions.uberdooX.adapters.AccountsAdapter;
import com.pyramidions.uberdooX.helpers.SharedHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AccountsFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static TextView userName, userMobile;
    public static ImageView profilePic;
    LinearLayout logOut;
    AppSettings appSettings;
    TextView logOutText;
    LinearLayout changeProfile;
    LinearLayout viewEditAddress;
    LinearLayout changePassword, guestLogin;

    private String mParam1;
    GoogleApiClient mGoogleApiClient;
    private String mParam2;
    MainActivity activity;
    private OnFragmentInteractionListener mListener;
    private List<Integer> allColors = new ArrayList<>();
    private ImageView changeThemIcon;
    private RecyclerView changeThemeAdapter;
    private boolean themeOpen;


    public AccountsFragment() {
        // Required empty public constructor
    }

    public static AccountsFragment newInstance(String param1, String param2) {
        AccountsFragment fragment = new AccountsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activity = (MainActivity) getContext();
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_accounts, container, false);
        //noinspection deprecation
        FacebookSdk.sdkInitialize(getApplicationContext());

        LinearLayout changeTheme = view.findViewById(R.id.changeTheme);
        changeThemIcon = view.findViewById(R.id.changeThemIcon);
        changeThemeAdapter = view.findViewById(R.id.changeThemeAdapter);

        ImageView account_profile = view.findViewById(R.id.account_profile);
        ImageView account_password = view.findViewById(R.id.account_password);
        ImageView account_address = view.findViewById(R.id.account_address);
        ImageView account_about = view.findViewById(R.id.account_about);
        ImageView account_theme = view.findViewById(R.id.account_theme);
        ImageView account_logout = view.findViewById(R.id.account_logout);

        Utils.setIconColour(activity, account_profile.getDrawable());
        Utils.setIconColour(activity, account_password.getDrawable());
        Utils.setIconColour(activity, account_address.getDrawable());
        Utils.setIconColour(activity, account_about.getDrawable());
        Utils.setIconColour(activity, account_theme.getDrawable());
        Utils.setIconColour(activity, account_logout.getDrawable());

        initViews(view);
        setData();

        themeOpen = false;
        allColors = Utils.getAllMaterialColors(activity);

        int numberOfColumns = 6;
        changeThemeAdapter.setLayoutManager(new GridLayoutManager(activity, numberOfColumns));
        AccountsAdapter adapter = new AccountsAdapter(activity, allColors);
        changeThemeAdapter.setAdapter(adapter);
        changeTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!themeOpen) {
                    expand(changeThemeAdapter);
                    themeOpen = true;
                } else {
                    collapse(changeThemeAdapter);
                    themeOpen = false;
                }
            }
        });
        return view;
    }

    public void expand(final View v) {
        changeThemIcon.setRotation(90);
        //changeThemeAdapter.setVisibility(View.VISIBLE);
        v.measure(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RecyclerView.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapse(final View v) {
        changeThemIcon.setRotation(0);
        //changeThemeAdapter.setVisibility(View.GONE);
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    private void setData() {
        if (appSettings.getUserType().equalsIgnoreCase("guest")) {
            userName.setText(R.string.hello);
            userMobile.setText(R.string.guest);
        } else {
            userName.setText(String.format("%s %s", MainActivity.userDetails
                    .optString("first_name"), MainActivity.userDetails.optString("last_name")));
            userMobile.setText(MainActivity.userDetails.optString("mobile"));

            Log.e("Accounts", "setData: " + appSettings.getUserImage()
                    + " " + appSettings.getUserName() + " " + appSettings.getUserNumber());
//            userName.setText(appSettings.getUserName());
//            userMobile.setText(appSettings.getUserNumber());

            Drawable drawable = getResources().getDrawable(R.drawable.dp);
            drawable.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(activity),
                    PorterDuff.Mode.SRC_IN));

            Glide.with(activity).load(MainActivity.userDetails.optString("image"))
                    .placeholder(drawable)
                    .dontAnimate()
                    .into(profilePic);

            // Utils.setIconColour(activity, profilePic.getDrawable());
        }

    }


    private void initViews(View view) {
        appSettings = new AppSettings(activity);

        userName = (TextView) view.findViewById(R.id.userName);
        changeProfile = (LinearLayout) view.findViewById(R.id.changeProfile);
        logOutText = (TextView) view.findViewById(R.id.logOutText);
        userMobile = (TextView) view.findViewById(R.id.userMobile);
        logOut = (LinearLayout) view.findViewById(R.id.logOut);
        profilePic = (ImageView) view.findViewById(R.id.account_profile_pic);
        Utils.setIconColour(activity, profilePic.getDrawable());
        guestLogin = (LinearLayout) view.findViewById(R.id.guestLogin);
        viewEditAddress = (LinearLayout) view.findViewById(R.id.viewEditAddress);
        View changePasswordView = (View) view.findViewById(R.id.changePasswordView);
        changePassword = (LinearLayout) view.findViewById(R.id.changePassword);

        if (appSettings.getIsSocialLogin().equalsIgnoreCase("true")) {
            changePassword.setVisibility(View.GONE);
            changePasswordView.setVisibility(View.GONE);

        } else {
            changePassword.setVisibility(View.VISIBLE);
            changePasswordView.setVisibility(View.VISIBLE);
        }
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                moveSignInActivity();
                try {
                    LoginManager.getInstance().logOut();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (mGoogleApiClient.isConnected()) {
                        Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                        mGoogleApiClient.disconnect();
                        mGoogleApiClient.connect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (appSettings.getUserType().equalsIgnoreCase("guest")) {
            guestLogin.setVisibility(View.GONE);
            logOutText.setText(getResources().getString(R.string.sign_in));
        } else {
            guestLogin.setVisibility(View.VISIBLE);
            logOutText.setText(getResources().getString(R.string.logout));
        }
        changeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, EditProfileActivity.class);
                intent.putExtra("provider_details", "" + MainActivity.userDetails);
                startActivity(intent);
            }
        });
        initGeneral(view);
        viewEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, AddressActivity.class);
                startActivity(intent);
            }
        });
        initGoogle();
    }

    private void initGoogle() {

        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

    }

    private void initGeneral(View view) {

        final LinearLayout faq, aboutUs, termsAndCondition;
        //faq = view.findViewById(R.id.faq);
        aboutUs = view.findViewById(R.id.aboutUs);
        //termsAndCondition = view.findViewById(R.id.termsAndCondition);


       /* faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, FaqActivity.class);
                startActivity(intent);
            }
        });
*/
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, AboutUs.class);
                startActivity(intent);
            }
        });

        /*termsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, TermsAndConditions.class);
                startActivity(intent);
            }
        });*/
    }

    private void moveSignInActivity() {
        appSettings.setIsLogged("false");
        Intent intent = new Intent(activity, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
