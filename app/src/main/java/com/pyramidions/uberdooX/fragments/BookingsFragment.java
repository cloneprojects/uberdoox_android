package com.pyramidions.uberdooX.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.pyramidions.uberdooX.Events.Status;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.activities.SignInActivity;
import com.pyramidions.uberdooX.adapters.BookingsAdapter;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static JSONArray bookingsArray = new JSONArray();
    private static String TAG = BookingsFragment.class.getSimpleName();
    RelativeLayout guest_login;
    public static RelativeLayout empty_layout;

    AppSettings appSettings;
    public static SwipeRefreshLayout swipeView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    public static RecyclerView bookingsList;

    public BookingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookingsFragment newInstance(String param1, String param2) {
        BookingsFragment fragment = new BookingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_bookings, container, false);
        initViews(view);

        return view;
    }

    public static void getData(final Context context) {

        ApiCall.getMethodHeaders(context, UrlHelper.VIEW_BOOKINGS, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.log(TAG, "responseValues: " + response);
                if (swipeView.isRefreshing()) {
                    swipeView.setRefreshing(false);
                }
                bookingsArray = response.optJSONArray("all_bookings");
                if (bookingsArray.length() > 0) {
                    empty_layout.setVisibility(View.GONE);
                    bookingsList.setVisibility(View.VISIBLE);
                    BookingsAdapter adapter = new BookingsAdapter(context, bookingsArray);
                    bookingsList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    empty_layout.setVisibility(View.VISIBLE);
                    bookingsList.setVisibility(View.GONE);

                }
            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStatus(Status event) {
        getData(getActivity());
    }


    private void initViews(View view) {
        appSettings = new AppSettings(getActivity());
        bookingsList = view.findViewById(R.id.bookingsList);
        empty_layout = view.findViewById(R.id.empty_layout);
        guest_login = view.findViewById(R.id.guest_login);
        swipeView = view.findViewById(R.id.swipeView);
        bookingsList.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(getActivity());
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (appSettings.getUserType().equalsIgnoreCase("guest")) {
            guest_login.setVisibility(View.VISIBLE);
            swipeView.setVisibility(View.GONE);
            empty_layout.setVisibility(View.GONE);
        } else {
            swipeView.setVisibility(View.VISIBLE);
            guest_login.setVisibility(View.GONE);
            getData(getActivity());


        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
