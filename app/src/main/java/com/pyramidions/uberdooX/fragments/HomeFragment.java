package com.pyramidions.uberdooX.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
import com.pyramidions.uberdooX.Events.Favorite;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.Volley.ApiCall;
import com.pyramidions.uberdooX.Volley.VolleyCallback;
import com.pyramidions.uberdooX.activities.AppSettings;
import com.pyramidions.uberdooX.adapters.MainCategoriesAdapter;
import com.pyramidions.uberdooX.adapters.SLiderAdapter;
import com.pyramidions.uberdooX.adapters.SlidingImage_Adapter;
import com.pyramidions.uberdooX.helpers.DatabaseHandler;
import com.pyramidions.uberdooX.helpers.UrlHelper;
import com.pyramidions.uberdooX.helpers.Utils;
import com.rd.PageIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView categoriesView;
    RecyclerViewPager viewpager;
    ViewPager dviewpager;
    private JSONArray faveList;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
//    public static TextView userName;
//    public static CircleImageView profilePic;


    private OnFragmentInteractionListener mListener;
    private String TAG = HomeFragment.class.getSimpleName();
    //    RelativeLayout cityButton;
    private JSONArray categoryDetailsArray = new JSONArray();
    private JSONArray bannerArray = new JSONArray();
    private PageIndicatorView circleIndicator;


    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFavorite(Favorite event) {
        Log.e(TAG, "onFavorite: ");
        setFavorite();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onFavorite: resume");
        setFavorite();
    }

    private void setFavorite() {

        DatabaseHandler db = new DatabaseHandler(getContext());
        faveList = db.getFavList();
        db.close();

        Log.e("HomeFragment", "bannerArray: " + bannerArray);
        Log.e("HomeFragment", "faveList: " + faveList);
        if (faveList.length() <= 0) {
            // bannerArray = response.optJSONArray("banner_images");

            if (bannerArray != null) {
                viewpager.setAdapter(new SLiderAdapter(getActivity(), bannerArray, false));
            }
        } else {
            // Log.e("db", "bannerArray: " + "inElse");
            viewpager.setAdapter(new SLiderAdapter(getActivity(), faveList, true));
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        faveList = new JSONArray();
        initViews(view);
        // initListners();
        //  initSLiders();


        AppSettings appSettings = new AppSettings(getActivity());
        getData();

        if (appSettings.getUserType().equalsIgnoreCase("guest")) {
//            userName.setText("Guest!");

        } else {
//            Glide.with(getActivity()).load(appSettings.getUserImage()).placeholder(getActivity().getResources().getDrawable(R.drawable.profile_pic)).into(profilePic);
        }
        return view;

    }

    private void initSLiders() {


    }

    private void initListners() {
//        cityButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showCityDialog();
//            }
//        });
    }

    private void getData() {
        categoryDetailsArray = new JSONArray();
        bannerArray = new JSONArray();
        ApiCall.getMethod(getActivity(), UrlHelper.HOME_DASH_BOARD, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                JSONArray list_category = new JSONArray();
                list_category = response.optJSONArray("list_category");
                JSONObject jsonObject = new JSONObject();
                jsonObject = list_category.optJSONObject(0);

                JSONArray jsonArray = jsonObject.names();
                for (int i = 0; i < jsonArray.length(); i++) {
                    String val = jsonArray.optString(i);
                    JSONObject jsonObject1 = new JSONObject();
                    try {
                        jsonObject1.put("name", val);

                        JSONArray jsonArray1 = new JSONArray();
                        jsonArray1 = jsonObject.optJSONArray(val);
                        jsonObject1.put("subCategories", jsonArray1);
                        categoryDetailsArray.put(jsonObject1);
                        Utils.log(TAG, "" + categoryDetailsArray.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                        LinearLayoutManager.VERTICAL, false);
                MainCategoriesAdapter adapter = new MainCategoriesAdapter(getActivity(), categoryDetailsArray);
                categoriesView.setLayoutManager(layoutManager);
                categoriesView.setAdapter(adapter);


                final LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                viewpager.setLayoutManager(layout);


                DatabaseHandler db = new DatabaseHandler(getContext());
                faveList = db.getFavList();
                db.close();
                //Log.e("db", "faveListsize: " + faveList.length());
                if (faveList.length() <= 0) {
                    bannerArray = response.optJSONArray("banner_images");
                    // Log.e("db", "bannerArray: " + bannerArray);
                    viewpager.setAdapter(new SLiderAdapter(getActivity(), bannerArray, false));
                } else {
                    // Log.e("db", "bannerArray: " + "inElse");
                    viewpager.setAdapter(new SLiderAdapter(getActivity(), faveList, true));
                }
                // DatabaseHandler db = new DatabaseHandler(getContext());
                //db.getFavList();

                // bannerArray = response.optJSONArray("banner_images");
                //viewpager.setAdapter(new SLiderAdapter(getActivity(), bannerArray, false));
                // circleIndicator.setViewPager(viewpager);
                dviewpager.setAdapter(new SlidingImage_Adapter(5, getActivity()));


                viewpager.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                        if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {
                            int c = layout.findFirstCompletelyVisibleItemPosition();
                            Log.d(TAG, "onScrollStateChanged: " + c);
                            dviewpager.setCurrentItem(c);
                            circleIndicator.setSelection(c);
                        }
                    }

                    @Override
                    public void onScrolled(final RecyclerView recyclerView, int i, int i2) {

                /*int visibleItemCount = recyclerView.getChildCount();
                Log.e("visibleItemCount",""+visibleItemCount);*/

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int childCount = viewpager.getChildCount();
                                int width = viewpager.getChildAt(0).getWidth();
                                int padding = (viewpager.getWidth() - width) / 2;
                                Log.e("padding", "" + padding);

                                for (int j = 0; j < childCount; j++) {
                                    View v = recyclerView.getChildAt(j);
                                    float rate = 0;
                                    Log.e("v.getLeft()", "" + v.getLeft());
                                    if (v.getLeft() <= padding) {
                                        if (v.getLeft() >= padding - v.getWidth()) {
                                            rate = (padding - v.getLeft()) * 1f / v.getWidth();
                                        } else {
                                            rate = 1;
                                        }
                                        v.setScaleY(1 - rate * 0.1f);
                                        v.setScaleX(1 - rate * 0.1f);

                                    } else {

                                        if (v.getLeft() <= recyclerView.getWidth() - padding) {
                                            rate = (recyclerView.getWidth() - padding - v.getLeft()) * 1f / v.getWidth();
                                        }
                                        v.setScaleY(0.9f + rate * 0.1f);
                                        v.setScaleX(0.9f + rate * 0.1f);
                                    }
                                }
                            }
                        });


                    }
                });


                viewpager.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (viewpager.getChildCount() < 3) {
                            if (viewpager.getChildAt(1) != null) {
                                if (viewpager.getCurrentPosition() == 0) {
                                    View v1 = viewpager.getChildAt(1);
                                    v1.setScaleY(0.9f);
                                    v1.setScaleX(0.9f);
                                } else {
                                    View v1 = viewpager.getChildAt(0);
                                    v1.setScaleY(0.9f);
                                    v1.setScaleX(0.9f);
                                }
                            }
                        } else {
                            if (viewpager.getChildAt(0) != null) {
                                View v0 = viewpager.getChildAt(0);
                                v0.setScaleY(0.9f);
                                v0.setScaleX(0.9f);
                            }
                            if (viewpager.getChildAt(2) != null) {
                                View v2 = viewpager.getChildAt(2);
                                v2.setScaleY(0.9f);
                                v2.setScaleX(0.9f);
                            }
                        }

                    }
                });


            }
        });


    }

    private void initViews(View view) {
        categoriesView = (RecyclerView) view.findViewById(R.id.categoriesView);
        viewpager = (RecyclerViewPager) view.findViewById(R.id.viewpager);
        dviewpager = (ViewPager) view.findViewById(R.id.dviewpager);
        circleIndicator = (PageIndicatorView) view.findViewById(R.id.circleIndicator);
        circleIndicator.setDynamicCount(true);
        //circleIndicator.setViewPager(dviewpager);


//        cityButton = (RelativeLayout) view.findViewById(R.id.cityButton);
//        userName = (TextView) view.findViewById(R.id.userName);
//        profilePic = (CircleImageView) view.findViewById(R.id.profilePic);
    }

    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void showCityDialog() {
        Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.search_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        EditText search = (EditText) dialog.findViewById(R.id.search);
        RecyclerView cityRecyclerView = (RecyclerView) dialog.findViewById(R.id.cityRecyclerView);
        dialog.show();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
