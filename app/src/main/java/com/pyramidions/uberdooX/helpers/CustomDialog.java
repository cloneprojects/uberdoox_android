package com.pyramidions.uberdooX.helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;

/*
 * Created by User on 11-02-2017.
 */


public class CustomDialog extends Dialog {
    @SuppressWarnings("ConstantConditions")
    public CustomDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


    }
}
