package com.pyramidions.uberdooX.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * Created by Poyyamozhi on 30-Apr-18.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Fave_Category_Db";
    private static final Integer DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "Fave_Category_Table";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
//            db.execSQL("create table Favorite (id integer primary key AUTOINCREMENT, categoryId text, subCategoryId text," +
//                    " categoryStatus text, categoryName text, categoryImage text, isLiked text )");
            db.execSQL("create table Favorite (id integer primary key AUTOINCREMENT, categoryId text, subCategoryId text," +
                    " categoryStatus text, categoryName text, categoryImage text)");
            // Log.e("db", "Table created successfully ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void insertSubData(String subCategoryId, String categoryName, String categoryImage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //contentValues.put("categoryId", categoryId);
        contentValues.put("subCategoryId", subCategoryId);
        contentValues.put("categoryName", categoryName);
        contentValues.put("categoryImage", categoryImage);
        contentValues.put("categoryStatus", "false");
        // Log.e("db", "insertData: " + contentValues);
        db.insert("Favorite", null, contentValues);
        db.close();
        //  Log.e("db", "result: " + result);
    }


    public void insertData(String categoryId, String categoryName, String categoryImage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("categoryId", categoryId);
        contentValues.put("categoryName", categoryName);
        contentValues.put("categoryImage", categoryImage);
        contentValues.put("categoryStatus", "true");
        // Log.e("db", "insertData: " + contentValues);
        db.insert("Favorite", null, contentValues);
        db.close();
        // Log.e("db", "result: " + result);
    }

    public JSONArray getFavList() {
        JSONArray jsonObjects = null;

        jsonObjects = new JSONArray();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from Favorite order by id desc", null);
        cursor.moveToNext();
        while (!cursor.isAfterLast()) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", cursor.getString(1));
                jsonObject.put("subId", cursor.getString(2));
                jsonObject.put("toSubCategory", cursor.getString(3));
                jsonObject.put("banner_name", cursor.getString(4));
                jsonObject.put("banner_logo", cursor.getString(5));

                jsonObjects.put(jsonObject);

                cursor.moveToNext();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //jsonObjects.add(jsonObject);
        cursor.close();
        sqLiteDatabase.close();
        Log.e("db", "getFavList: " + jsonObjects);
        // Log.e("db", "getFavListSize: " + jsonObjects.length());
        return jsonObjects;
    }


    public boolean isLiked(String id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        if (status.equalsIgnoreCase("true")) {
            cursor = db.rawQuery("SELECT * FROM Favorite where categoryId =" + id, null);
        } else {
            cursor = db.rawQuery("SELECT * FROM Favorite where subCategoryId =" + id, null);

        }
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count > 0;
    }

    public void deleteData(String id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (status.equalsIgnoreCase("true")) {
            db.delete("Favorite", "categoryId =? ", new String[]{id});
        } else {
            db.delete("Favorite", "subCategoryId =? ", new String[]{id});
        }
        db.close();
    }
}