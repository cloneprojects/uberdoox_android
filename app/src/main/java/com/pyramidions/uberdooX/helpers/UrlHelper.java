package com.pyramidions.uberdooX.helpers;

/**
 * Created by user on 23-10-2017.
 */

public class UrlHelper {

    public static String SOCKET_URL = "http://104.131.74.144:3000/";
    private static String BASE = "http://104.131.74.144/";
    public static String BASE_URL = BASE + "uber_test/public/api/";
    public static String SIGN_UP = BASE_URL + "signup";
    public static String SIGN_IN = BASE_URL + "userlogin";
    public static String FORGOT_PASSWORD = BASE_URL + "forgotpassword";
    public static String CHECK_OTP = BASE_URL + " ";
    public static String RESET_PASSWORD = BASE_URL + "resetpassword";
    public static String HOME_DASH_BOARD = BASE_URL + "homedashboard";
    public static String APP_SETTINGS = BASE_URL + "appsettings";

    public static String LIST_ADDRESS = BASE_URL + "listaddress";
    public static String ADD_ADDRESS = BASE_URL + "addaddress";
    public static String LIST_PROVIDERS = BASE_URL + "listprovider";
    public static String NEW_BOOKINGS = BASE_URL + "newbooking";
    public static String VIEW_BOOKINGS = BASE_URL + "view_bookings";
    public static String GET_PROVIDER_LOCATION = BASE_URL + "getproviderlocation";
    public static String VIEW_PROFILE = BASE_URL + "viewprofile";
    public static String UPDATE_DEVICE_TOKEN = BASE_URL + "updatedevicetoken";

    public static String REVIEW = BASE_URL + "review";
    public static String PAYMENT = BASE_URL + "pay";
    public static String PAYMENT_METHODS = BASE_URL + "list_payment_methods";
    public static String STRIPE_PAYMENT = BASE_URL + "stripe";
    public static String CHANGE_PASSWORD = BASE_URL + "changepassword";
    public static String LIST_SUB_CATEGORY = BASE_URL + "list_subcategory";
    public static String UPLOAD_IMAGE = BASE + "uber_test/public/admin/imageupload";
    public static String CANCEL_BOOKING = BASE_URL + "cancelbyuser";

    //104.131.74.144/uber_test/public/admin/showPag
    public static String ABOUT_US = BASE + "uber_test/public/admin/showPag";
    public static String FAQ = BASE + "uber_test/public/admin/faq";
    public static String TERMS = BASE + "uber_test/public/admin/terms";
    public static String UPDATE_PROFILE = BASE_URL + "updateprofile";
    public static String DELETE_ADDRESS = BASE_URL + "deleteaddress";
    public static String UPDATE_ADDRESS = BASE_URL + "updateaddress";
    public static String SOCIAL_LOGIN = BASE_URL + "sociallogin";
}