package com.pyramidions.uberdooX.helpers;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.pyramidions.uberdooX.R;
import com.pyramidions.uberdooX.activities.ChangePasswordActivity;
import com.pyramidions.uberdooX.activities.SignUpActivity;
import com.pyramidions.uberdooX.application.Uberdoo;


import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/*
 * Created by user on 23-10-2017.
 */

public class Utils {

    private static CustomDialog customDialog;
    public static Boolean isShowing = false;
    private String TAG = Utils.class.getSimpleName();

    public static boolean checkGpsisEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }

    };

    public static class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new AsteriskPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public static int getPrimaryDark(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimaryDark, value, true);
        return value.data;
    }

    public static void setIconColour(Context context, Drawable drawable) {
        drawable.setColorFilter(new PorterDuffColorFilter(getPrimaryCOlor(context), PorterDuff.Mode.SRC_IN));
    }

    public static void setRLColour(Context context, RelativeLayout relativeLayout) {
        relativeLayout.setBackgroundColor(getPrimaryCOlor(context));
    }

    public static void setBacColour(Context context, ImageView drawable) {
        Drawable drawable1 = context.getResources().getDrawable(R.drawable.circle_violeet_transparent_2);
        drawable1.setColorFilter(new PorterDuffColorFilter(getPrimaryCOlor(context), PorterDuff.Mode.SRC_IN));
        drawable.setImageDrawable(drawable1);
    }

    public static void setTextColour(Context context, TextView textView) {
        textView.setTextColor(getPrimaryCOlor(context));
    }

    public static void setDrawabe(Context context, ImageView profilePic) {
        Drawable drawable = context.getResources().getDrawable(R.drawable.dp);
        drawable.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(context),
                PorterDuff.Mode.SRC_IN));
        profilePic.setImageDrawable(drawable);
    }

    public static void setViewDot(Context context, ImageView profilePic) {
        Drawable drawable = context.getResources().getDrawable(R.drawable.circle_violeet_transparent);
        drawable.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(context),
                PorterDuff.Mode.SRC_IN));
        profilePic.setImageDrawable(drawable);
    }

    public static Drawable getDrawable(Context context) {
        Drawable drawable = context.getResources().getDrawable(R.drawable.dp);
        drawable.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(context),
                PorterDuff.Mode.SRC_IN));
        return drawable;
    }

    public static void setButtonColor(Context context, Button button) {
        Drawable drawable1 = context.getResources().getDrawable(R.drawable.black_button);
        drawable1.setColorFilter(new PorterDuffColorFilter(Utils.getPrimaryCOlor(context), PorterDuff.Mode.SRC_IN));
        button.setBackground(drawable1);
    }

    public static void setButton(Context context, Button button) {
        button.setBackgroundColor(Utils.getPrimaryCOlor(context));
    }

    public static void setStripColor(Context context, NavigationTabStrip drawable) {
        drawable.setStripColor(getPrimaryCOlor(context));
    }


    public static void Setheme(Context activity, String themevalue) {
        switch (themevalue) {
            case "1":
                activity.setTheme(R.style.AppThemeMain);
                break;
            case "2":
                activity.setTheme(R.style.AppThemePink);
                break;
            case "3":
                activity.setTheme(R.style.AppThemeBlue);
                break;
            case "4":
                activity.setTheme(R.style.AppThemeYellow);
                break;
            case "5":
                activity.setTheme(R.style.AppThemeIndigo);
                break;
            case "6":
                activity.setTheme(R.style.AppThemeRed);
                break;

            default:
                activity.setTheme(R.style.AppThemeMain);
                break;
        }
    }


    public static List<Integer> getAllMaterialColors(Context activity) {
        XmlResourceParser xrp = activity.getResources().getXml(R.xml.theme_color);
        List<Integer> allColors = new ArrayList<>();
        int nextEvent;
        try {
            while ((nextEvent = xrp.next()) != XmlResourceParser.END_DOCUMENT) {
                String s = xrp.getName();
                if ("color".equals(s)) {
                    String color = xrp.nextText();
                    allColors.add(Color.parseColor(color));
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allColors;
    }

    public static void log(String TAG, String content) {
        Log.d(TAG, "" + content);
    }

    public static void toast(Context context, String toastmsg) {

        Toast.makeText(context, toastmsg, Toast.LENGTH_SHORT).show();
    }

    public static void show(Context context) {
        if (!isShowing) {
            isShowing = true;
            customDialog = new CustomDialog(context);
            customDialog.setCancelable(false);
            customDialog.setCanceledOnTouchOutside(false);
            customDialog.setContentView(R.layout.custom_dialog);
            Window window = customDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            //  window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 0);
            customDialog.show();
        }


    }

    public static void dismiss() {
        if (isShowing) {
            isShowing = false;
            try {
                customDialog.dismiss();
            } catch (Exception e) {

            }
        }
    }

    private void uploadImageFile(File f) {


    }


    public static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "ProfilePic", "UberX");
        return Uri.parse(path);
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromUriNew(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


}
